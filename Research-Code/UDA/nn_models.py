#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Define any discriminator functions used here. The discriminator arg_scope is too defined here
@author: her171
"""

from __future__ import division
import tensorflow as tf

from extra_layers import leaky_relu, noise
from tensorflow.contrib.framework import add_arg_scope


slim = tf.contrib.slim


# ------------------------------------------------------------------------------
# Classifier ArgScope
# ------------------------------------------------------------------------------
def classifier_arg_scope():

    # Argscope 1
    #    with slim.arg_scope([slim.fully_connected, slim.conv2d],
    #                        weights_initializer=tf.truncated_normal_initializer(stddev=0.02),
    #                        weights_regularizer=slim.l2_regularizer(0.0001))  :
    #
    #        with slim.arg_scope([slim.batch_norm],
    #                             decay=0.99,
    #                             epsilon=1e-5,
    #                             scale=True) as classifier_arg_scope :

    # Argscope 2
    with slim.arg_scope([slim.fully_connected, slim.conv2d],
                        weights_regularizer=slim.l2_regularizer(0.0)) as classifier_arg_scope : 

        return classifier_arg_scope


# ------------------------------------------------------------------------------
# Classifier Model
# ------------------------------------------------------------------------------
def classifier_function_mid_feats(inputs,
                                  is_training=True,
                                  reuse=False,
                                  use_instance_norm=False,
                                  num_classes=9):
   
    print('------------------------------------------------------------------')
    print('using the model without bottleneck layer with leaky relus')
    print('------------------------------------------------------------------')
    
    with tf.variable_scope('cnn', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.1):
            with slim.arg_scope([slim.dropout], is_training=is_training):
                with slim.arg_scope([slim.batch_norm], decay=0.99, is_training=is_training, scale=True):
                    with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu, normalizer_fn=slim.batch_norm):

                        if use_instance_norm:
                            net = tf.contrib.layers.instance_norm(inputs, scope='pre_process_inputs')
                            net = slim.conv2d(net, 64, [3, 3], 1, scope='conv1')
                        else:
                            net = slim.conv2d(inputs, 64, [3, 3], 1, scope='conv1')

                        net = slim.conv2d(net, 64, [3, 3], 1, scope='conv2')
                        net = slim.conv2d(net, 64, [3, 3], 1, scope='conv2_1')
                        net = slim.max_pool2d(net, [2, 2], scope='pool2')
                        net = slim.dropout(net, 0.5, scope='dropout2')
                        net = noise(net, 1.0, phase=is_training, scope='noise2')

                        net = slim.conv2d(net, 64, [3, 3], 1, scope='conv3')
                        net = slim.conv2d(net, 64, [3, 3], 1, scope='conv3_1')
                        net = slim.conv2d(net, 64, [3, 3], 1, scope='conv3_2')
                        net = slim.max_pool2d(net, [2, 2], scope='pool3')
                        net = slim.dropout(net, 0.5, scope='dropout3')
                        net = noise(net, 0.0001, phase=is_training, scope='noise3')

                        # Depreciated pooling. Moved to the Siamese modle
                        # ------------------------------------------------------
                        # Pooled tapping layer
                        # net_pooled = tf.reduce_mean(net, [1, 2], name='pool3_out', keep_dims=True)
                        # ------------------------------------------------------

                        net2 = slim.conv2d(net, 64, [3, 3], 1, scope='conv4')
                        net2 = slim.conv2d(net2, 64, [3, 3], 1, scope='conv4_1')
                        net2 = slim.conv2d(net2, 64, [3, 3], 1, scope='conv4_2')
                        net2 = tf.reduce_mean(net2, [1, 2], name='pool4', keep_dims=True)

                        net2 = tf.squeeze(net2, [1, 2], name='SpatialSqueeze')
                        logits = slim.fully_connected(net2, num_classes, scope='fc5')

                        return logits, net


# ------------------------------------------------------------------------------
# Confusion Model ArgScope
# ------------------------------------------------------------------------------
def confusion_model_arg_scope():
    
    # Argscope 1
    #    with slim.arg_scope([slim.conv2d, slim.fully_connected],
    #                        weights_initializer=tf.truncated_normal_initializer(stddev=0.02),
    #                        weights_regularizer=slim.l2_regularizer(0.0001))  :
    #
    #        with slim.arg_scope([slim.batch_norm],
    #                             decay=0.9,
    #                             epsilon=1e-5,
    #                             scale=True) as conf_arg_scope :

    with slim.arg_scope([slim.conv2d, slim.fully_connected],
                        weights_regularizer=slim.l2_regularizer(0.0)) as conf_arg_scope :

            return conf_arg_scope


# ------------------------------------------------------------------------------
# Confusion Model
# ------------------------------------------------------------------------------
@add_arg_scope
def confusion_function_1(inputs,
                         is_training=True,
                         use_compact_feats=True,
                         reuse=False):

    with tf.variable_scope('fully_convolutional_network', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.1):
            with slim.arg_scope([slim.batch_norm], decay=0.99, is_training=is_training, scale=True):
                with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu, normalizer_fn=None):

                    # Generates the confused outputs from the input features
                    net = slim.conv2d(inputs, 64, [3, 3], 1, scope='conf_conv1')
                    net = slim.conv2d(net, 64, [3, 3], 1, scope='conf_conv1_1')
                    if use_compact_feats:
                        net = slim.conv2d(net, 64, [3, 3], 1, scope='conf_conv1_2', activation_fn=tf.nn.tanh)
                    else:
                        net = slim.conv2d(net, 64, [3, 3], 1, scope='conf_conv1_2')
                    net = tf.reduce_mean(net, [1, 2], name='conf_pool1', keep_dims=True)

                    return net


@add_arg_scope
def confusion_function_2(inputs,
                         is_training=True,
                         use_compact_feats=True,
                         reuse=False):

    with tf.variable_scope('fully_convolutional_network', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.1):
            with slim.arg_scope([slim.batch_norm], decay=0.99, is_training=is_training, scale=True):
                with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu, normalizer_fn=None):

                    # Generates the confused outputs from the input features
                    net = slim.conv2d(inputs, 64, [3, 3], 1, scope='conf_conv1')
                    if use_compact_feats:
                        net = slim.conv2d(net, 64, [3, 3], 1, scope='conf_conv1_1', activation_fn=tf.nn.tanh)
                    else:
                        net = slim.conv2d(net, 64, [3, 3], 1, scope='conf_conv1_1')
                    net = tf.reduce_mean(net, [1, 2], name='conf_pool1', keep_dims=True)

                    return net
                
                
@add_arg_scope
def confusion_function_3(inputs,
                         is_training=True,
                         use_compact_feats=False,
                         confused_dim=64,
                         num_confusion_layers=3,
                         reuse=False):

    with tf.variable_scope('fully_convolutional_network', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.1):
            with slim.arg_scope([slim.batch_norm], decay=0.99, is_training=is_training, scale=True):
                with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu, normalizer_fn=None):
                    
                    # ---------------------------------------------------------
                    # Decide the activation after confusion
                    # ---------------------------------------------------------
                    if use_compact_feats:
                        activation_fn = tf.nn.tanh
                    else:
                        activation_fn = leaky_relu
                    
                    if num_confusion_layers == 1:
                        net = slim.conv2d(inputs, confused_dim, [3, 3], 1, scope='conf_conv1_0', activation_fn=activation_fn)
                    else:
                        net = slim.conv2d(inputs, confused_dim, [3, 3], 1, scope='conf_conv1_0')
                        for i in range(num_confusion_layers-2):
                            net = slim.conv2d(net, confused_dim, [3, 3], 1, scope='conf_conv1_' + str(i+1))
                        
                        net = slim.conv2d(net, confused_dim, [3, 3], 1, scope='conf_conv1_' + str(num_confusion_layers-1), activation_fn=activation_fn)
                        
                    net = tf.reduce_mean(net, [1, 2], name='conf_pool1', keep_dims=True)

                    return net


@add_arg_scope
def confusion_function_30(inputs,
                          is_training=True,
                          use_compact_feats=False,
                          confused_dim=64,
                          num_confusion_layers=3,
                          reuse=False):
    with tf.variable_scope('fully_convolutional_network', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.1):
            with slim.arg_scope([slim.batch_norm], decay=0.99, is_training=is_training, scale=True):
                with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu, normalizer_fn=None):

                    # ---------------------------------------------------------
                    # Decide the activation after confusion
                    # ---------------------------------------------------------
                    if use_compact_feats:
                        activation_fn = tf.nn.tanh
                    else:
                        activation_fn = leaky_relu

                    if num_confusion_layers == 1:
                        net = slim.conv2d(inputs, confused_dim, [3, 3], 1, scope='conf_conv1_0',
                                          activation_fn=activation_fn)
                    else:
                        net = slim.conv2d(inputs, 64, [3, 3], 1, scope='conf_conv1_0')
                        for i in range(num_confusion_layers - 2):
                            net = slim.conv2d(net, 64, [3, 3], 1, scope='conf_conv1_' + str(i + 1))

                        net = slim.conv2d(net, confused_dim, [3, 3], 1,
                                          scope='conf_conv1_' + str(num_confusion_layers - 1),
                                          activation_fn=activation_fn)

                    net = tf.reduce_mean(net, [1, 2], name='conf_pool1', keep_dims=True)

                    return net











@add_arg_scope
def confusion_function_4(inputs,
                         is_training=True,
                         use_compact_feats=False,
                         confused_dim=64,
                         num_confusion_layers=3,
                         reuse=False):

    print('------------------------------------------------------------------')
    print('skip confusion function defined')
    print('------------------------------------------------------------------')
    assert inputs.shape[3] == confused_dim, 'Residual confusin :the input and output dimensions of the confusion function should be same'

    with tf.variable_scope('fully_convolutional_network', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.1):
            with slim.arg_scope([slim.batch_norm], decay=0.99, is_training=is_training, scale=True):
                with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu, normalizer_fn=None):

                    # ---------------------------------------------------------
                    # Decide the activation after confusion
                    # ---------------------------------------------------------
                    if use_compact_feats:
                        activation_fn = tf.nn.tanh
                    else:
                        activation_fn = leaky_relu

                    if num_confusion_layers == 1:
                        net = slim.conv2d(inputs, confused_dim, [3, 3], 1, scope='conf_conv1_0',
                                          activation_fn=None)
                    else:
                        net = slim.conv2d(inputs, confused_dim, [3, 3], 1, scope='conf_conv1_0')
                        for i in range(num_confusion_layers - 2):
                            net = slim.conv2d(net, confused_dim, [3, 3], 1, scope='conf_conv1_' + str(i + 1))

                        net = slim.conv2d(net, confused_dim, [3, 3], 1,
                                          scope='conf_conv1_' + str(num_confusion_layers - 1),
                                          activation_fn=None)

                    # ---------------------------------------------------------
                    # Residual connections
                    # ---------------------------------------------------------
                    net = net + inputs

                    # ---------------------------------------------------------
                    # Decide the activation after confusion
                    # ---------------------------------------------------------
                    if use_compact_feats:
                        activation_fn = tf.nn.tanh
                    else:
                        activation_fn = leaky_relu

                    net = activation_fn(net)
                    net = tf.reduce_mean(net, [1, 2], name='conf_pool1', keep_dims=True)

                    return net











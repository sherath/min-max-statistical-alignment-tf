#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 15:11:57 2018

@author: her171
"""

from __future__ import division
import numpy as np
import time
import random

import tensorflow as tf
from tensorflow.python import debug as tf_debug
slim = tf.contrib.slim


class Train_Eval :
    
    # --------------------------------------------------------------------------
    # Training Opts defining and Training and Evaluation Method
    # --------------------------------------------------------------------------
        
    def train_eval(self):

        # fix the random seed for the experiment-------------------------------
        tf.set_random_seed(0)
        np.random.seed(0)
        random.seed(0)       
        # ----------------------------------------------------------------------

        if self.train_params['debug_mode']:
            self.sess = tf_debug.LocalCLIDebugWrapperSession(self.sess)
            self.sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)

        # ----------------------------------------------------------------------
        # Observe gradients of a given loss w.r.t a given parameter
        # ----------------------------------------------------------------------
        with tf.name_scope('gradient_observer'):
            observer_loss_name_list = ['source_train', 'target_train', 'stat_align_loss']
            observe_loss_list = [self.source_train_loss,
                                 self.target_train_loss_weighted,
                                 self.scaled_stat_align_loss]
            
            observe_var_name_list = ['cnn/conv3_2/weights']
            observe_var_list = []
            with tf.variable_scope("base_network", reuse=True):
                for obs_var_name in observe_var_name_list:
                    observe_var_list.append(tf.get_variable(obs_var_name))
    
            get_grad_list = []
            dName = []  # Name of the derivative
            for obs_loss, obs_loss_name in zip(observe_loss_list, observer_loss_name_list):
                for obs_var, obs_var_name in zip(observe_var_list, observe_var_name_list):
                    dName.append(obs_loss_name + '-' + obs_var_name)
                    get_grad_list.append(tf.gradients(obs_loss, obs_var))

        with tf.variable_scope('optimizers_and_train_steps'):
                   
            if self.train_params['solver_type'] == 'adam':
                optimizer_basenet = tf.train.AdamOptimizer(self.train_params['learning_rate'], 0.5, epsilon=1e-03, name='opt_basenet')

            elif self.train_params['solver_type'] == 'rmsp':
                optimizer_basenet = tf.train.RMSPropOptimizer(self.train_params['learning_rate'], name='opt_basenet')

        # ----------------------------------------------------------------------
        # Define the train ops
        # ----------------------------------------------------------------------

            # ------------------------------------------------------------------
            # Basenet Train Ops
            # ------------------------------------------------------------------
            basenet_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope=self.train_params['base_network_update_op_scope'])
            sam_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope=self.train_params['sam_update_op_scope'])
            full_model_update_ops = basenet_update_ops + sam_update_ops

            print(' ')
            print('================= basenet + sam update ops =====================')
            for uop in full_model_update_ops:
                print(uop.op.name)
            print('================================================================')
            
            basenet_global_step = tf.get_variable('basenet_global_step', [], initializer=tf.constant_initializer(0.0), trainable=False)
            basenet_train_step = slim.learning.create_train_op(self.base_net_train_loss,
                                                               optimizer_basenet,
                                                               variables_to_train=self.basenet_variables+self.sam_variables,
                                                               global_step=basenet_global_step,
                                                               update_ops=full_model_update_ops)

        # ----------------------------------------------------------------------
        # Load source and target data
        # ----------------------------------------------------------------------
        source_data = self.src_data.train_set['x']
        source_labels_1h = self.src_data.train_set['y'] 
        source_labels = np.argmax(source_labels_1h, axis=1)
        
        target_data = self.trg_data.test_set['x']
        target_labels_1h = self.trg_data.test_set['y'] 
        target_labels = np.argmax(target_labels_1h, axis=1)
 
        # ----------------------------------------------------------------------
        # Decide the training duration
        # ----------------------------------------------------------------------
        number_of_epochs = self.train_params['number_of_epochs']
        train_batch_size = self.train_params['train_batch_size']
        val_batch_size = self.train_params['val_batch_size']
        
        # Load image paths and labels
     
        train_indices_array = np.arange(len(source_labels)) 
        val_indices_array = np.arange(len(target_labels))
        val_indices_array_for_training = np.arange(len(target_labels))

        # ----------------------------------------------------------------------
        # Initialize the model 
        # ----------------------------------------------------------------------
        self.sess.run(tf.local_variables_initializer())
        self.sess.run(tf.global_variables_initializer())

        # ----------------------------------------------------------------------
        # Initialize the summary writer
        # ----------------------------------------------------------------------
        self.writer_train = tf.summary.FileWriter(self.train_params['summaries_dir']+'/train', self.sess.graph)
        self.writer_val = tf.summary.FileWriter(self.train_params['summaries_dir']+'/val', self.sess.graph)
        # self.writer_grad = tf.summary.FileWriter(self.train_params['summaries_dir']+'/grad', self.sess.graph)
        
        # Iterate through the epochs
        for ep in range(number_of_epochs):
 
            # Batching function
            def batching(samples, batch_size):
                return (samples[pos:pos+batch_size] for pos in xrange(0, len(samples), batch_size))

            np.random.shuffle(val_indices_array)
            np.random.shuffle(train_indices_array) 

            shuffled_val_indices = list(val_indices_array)
            shuffled_train_indices = list(train_indices_array)
            
            # -----------------------------------------------------------------
            # Validation function
            # -----------------------------------------------------------------

            # Iterate through the batches
            print('-----------------------------')
            print('validation on epoch ' + str(ep))
            print('-----------------------------')
            batch_number, evaluated_sample_count = 0, 0
            epoch_target_val_acc, epoch_target_val_loss = 0.0, 0.0
            
            for batch_indices in batching(shuffled_val_indices, val_batch_size):
                
                batch_start_time = time.time()
                batch_number += 1
                sample_count = len(batch_indices)
                total_count = evaluated_sample_count + sample_count
            
                target_image_batch = target_data[batch_indices, :, :, :]
                target_image_labels = target_labels[batch_indices]
            
                target_image_batch_float = np.array(target_image_batch).astype(np.float32)

                target_val_acc_, target_val_loss_ =  \
                    self.sess.run([self.target_val_acc,    # 1. Target train accuracy
                                  self.target_val_loss],   # 2. Target crossentropy loss
                                  feed_dict={self.trg_x: target_image_batch_float,
                                             self.image_labels: target_image_labels})

                epoch_target_val_acc = (target_val_acc_*sample_count + epoch_target_val_acc*evaluated_sample_count)/total_count
                epoch_target_val_loss = (target_val_loss_*sample_count + epoch_target_val_loss*evaluated_sample_count)/total_count

                evaluated_sample_count = total_count + 0  # Added zero to avoid any mutative assignments

                if batch_number % self.train_params['display_interval'] == 0:
                        print('val epoch :' + str(ep) + ', processing batch :' + str(batch_number) +
                              ', in ' + str(round(time.time()-batch_start_time, 4)) + 's ' +
                              ', target acc  : ' + str(round(epoch_target_val_acc, 4)) +
                              ', target loss : ' + str(round(epoch_target_val_loss, 4)))
                
                if sample_count < val_batch_size:
                    print('Evaluated a smaller batch : the batch size in batch ' + str(batch_number) + ' is ' + str(sample_count))

            epoch_target_val_acc_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_accuracies', simple_value=epoch_target_val_acc)])
            epoch_target_val_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_losses', simple_value=epoch_target_val_loss)])

            val_scalar_summaries_list = [epoch_target_val_acc_summary, epoch_target_val_loss_summary]

            for temp_val_scalar_summary in val_scalar_summaries_list:
                self.writer_val.add_summary(temp_val_scalar_summary, ep)

            # -----------------------------------------------------------------
            # Training function
            # -----------------------------------------------------------------
            print('-----------------------------')
            print('training on epoch ' + str(ep))
            print('-----------------------------')
            batch_number, trained_sample_count = 0, 0

            epoch_base_net_train_loss, epoch_source_train_acc, epoch_source_train_loss = 0.0, 0.0, 0.0
            epoch_stat_align_loss = 0.0
            epoch_gradients = [0.0]*len(dName)
            
            for batch_indices in batching(shuffled_train_indices, train_batch_size):

                # Skip small batches in the training
                if len(batch_indices) < train_batch_size:
                    print('important : skipped the last batch of size ' + str(len(batch_indices)))
                    continue                

                batch_start_time = time.time()
                batch_number += 1
                sample_count = len(batch_indices)
                total_count = trained_sample_count + sample_count

                source_image_batch = source_data[batch_indices, :, :, :]
                source_image_labels = source_labels[batch_indices]
            
                # --------------------------------------------------------------
                # Select a random target image set                
                # --------------------------------------------------------------
                np.random.shuffle(val_indices_array_for_training) 
                shuffled_val_indices_array_for_training = list(val_indices_array_for_training)
                random_target_indices = shuffled_val_indices_array_for_training[0:train_batch_size]
                
                target_image_batch = target_data[random_target_indices, :, :, :]
            
                # Convert the data to float
                source_image_batch_float = np.array(source_image_batch).astype(np.float32)
                target_image_batch_float = np.array(target_image_batch).astype(np.float32)

                # --------------------------------------------------------------
                # Training session call
                # --------------------------------------------------------------
                if ep >= self.train_params['gen_train_delay']:
                    [gamma_s_cls_, gamma_t_cls_, gamma_sam_] = [1.0, 1.0, 1.0]
                else:
                    [gamma_s_cls_, gamma_t_cls_, gamma_sam_] = [1.0, 0.0, 0.0]

                # 1      2                      3                   4           5
                _, stat_align_loss_, source_train_acc_, source_train_loss_, sam_sanity_ =  \
                    self.sess.run([basenet_train_step,                        # 1. Basenet Train Step
                                  self.stat_align_loss,                       # 2. Stats align module loss
                                  self.source_train_acc,                      # 3. Source data classification accuracy
                                  self.source_train_loss,                     # 4. Source data classification cross entropy loss
                                  self.sam_sanity_check],                     # 5. Sanity check operator for sam
                                  feed_dict={self.src_x: source_image_batch_float,
                                             self.trg_x: target_image_batch_float,
                                             self.image_labels: source_image_labels,
                                             self.gamma_s_cls: gamma_s_cls_,
                                             self.gamma_t_cls: gamma_t_cls_,
                                             self.gamma_sam: gamma_sam_})

                # -------------------------------------------------------------
                # Sanity checker
                # -------------------------------------------------------------
                if np.random.rand() > 0.9:
                    if self.train_params['checked_sanity'] != 'None':
                        print('checked  sanity : ' + self.train_params['checked_sanity'])
                        print(sam_sanity_)
                        if type(sam_sanity_) is np.ndarray:
                            print('shape : ' + str(sam_sanity_.shape))
                            print('rank : ' + str(np.linalg.matrix_rank(sam_sanity_)))
                            if len(sam_sanity_.shape) > 1:
                                print('trace :' + str(np.trace(sam_sanity_)))
                                if sam_sanity_.shape[0] == sam_sanity_.shape[1]:
                                    print('determinant :' + str(np.linalg.det(sam_sanity_)))

                # -------------------------------------------------------------
                # Update SAM module moments accumulation variables
                # -------------------------------------------------------------
                if ep >= self.train_params['gen_train_delay']:
                    # 1
                    _ = \
                        self.sess.run([self.update_moments],  # Updated the moments accumulation variables in the SAM module
                                      feed_dict={self.src_x: source_image_batch_float,
                                                 self.trg_x: target_image_batch_float})

                # --------------------------------------------------------------
                # Compute some losses 
                # --------------------------------------------------------------
                
                # Keep all switches on for basenet training loss computation
                [gamma_s_cls_, gamma_t_cls_, gamma_sam_] = [1.0, 1.0, 1.0]

                [base_net_train_loss_] =\
                    self.sess.run([self.base_net_train_loss],
                                  feed_dict={self.src_x: source_image_batch_float,
                                             self.trg_x: target_image_batch_float,
                                             self.image_labels: source_image_labels,
                                             self.gamma_s_cls: gamma_s_cls_,
                                             self.gamma_t_cls: gamma_t_cls_,
                                             self.gamma_sam: gamma_sam_})

                epoch_base_net_train_loss = (base_net_train_loss_*sample_count + epoch_base_net_train_loss*trained_sample_count)/total_count
                epoch_source_train_acc = (source_train_acc_*sample_count + epoch_source_train_acc*trained_sample_count)/total_count
                epoch_source_train_loss = (source_train_loss_*sample_count + epoch_source_train_loss*trained_sample_count)/total_count
                epoch_stat_align_loss = (stat_align_loss_*sample_count + epoch_stat_align_loss*trained_sample_count)/total_count

                trained_sample_count = total_count + 0  # Added zero to avoid any mutative assignments

                if batch_number % self.train_params['display_interval'] == 0:
                        print('train epoch :' + str(ep) + ', processing batch :' + str(batch_number) +
                              ', in ' + str(round(time.time()-batch_start_time, 4)) + 's ' +
                              ', source acc  : ' + str(round(epoch_source_train_acc, 4)) +
                              ', source loss : ' + str(round(epoch_source_train_loss, 4)) +
                              ', stat loss   : ' + str(round(epoch_stat_align_loss, 4)))

                # --------------------------------------------------------------
                # Observe gradients
                # --------------------------------------------------------------
                grad_list_ =\
                    self.sess.run(get_grad_list,
                                  feed_dict={self.src_x: source_image_batch_float,
                                             self.trg_x: target_image_batch_float,
                                             self.image_labels: source_image_labels})

                grad_norm_list = []
                for g_var in grad_list_:
                    grad_norm_list.append(np.linalg.norm(g_var))            

                epoch_gradients = [(g_norm_element_*sample_count + epoch_g_norm_element_*trained_sample_count)/total_count \
                                   for g_norm_element_, epoch_g_norm_element_ in zip(grad_norm_list, epoch_gradients)]

            # Write gradient summaries-----------------------------------------
            
            for derivative_name, derivative_value in zip(dName, epoch_gradients):
                derivative_summary_name = 'derivatives/'+derivative_name
                self.writer_train.add_summary(tf.Summary(value=[tf.Summary.Value(tag=derivative_summary_name,
                                                                                 simple_value=derivative_value)]), ep)

            # Write usual summaries--------------------------------------------
            
            epoch_base_net_train_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='train_losses/basenet_train_loss', simple_value=epoch_base_net_train_loss)])
            epoch_source_train_acc_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_accuracies', simple_value=epoch_source_train_acc)])
            epoch_source_train_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_losses', simple_value=epoch_source_train_loss)])
            epoch_stat_align_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='statistical_align_loss', simple_value=epoch_stat_align_loss)])

            epoch_stat_loss_weight_summary = tf.Summary(value=[tf.Summary.Value(tag='parameters/statistical_align_loss_alpha_weight', simple_value=self.train_params['alpha_weight_sam'])])
            epoch_tgt_cls_loss_weight_summary = tf.Summary(value=[tf.Summary.Value(tag='parameters/target_cls_loss_alpha_weight', simple_value=self.train_params['alpha_weight_t_cls'])])

            train_scalar_summaries_list = [epoch_base_net_train_loss_summary, epoch_stat_align_loss_summary,
                                           epoch_source_train_acc_summary, epoch_source_train_loss_summary,
                                           epoch_stat_loss_weight_summary, 
                                           epoch_tgt_cls_loss_weight_summary]

            for temp_train_scalar_summary in train_scalar_summaries_list:
                self.writer_train.add_summary(temp_train_scalar_summary, ep)

            # ------------------------------------------------------------------
            # Save Checkpoint
            # ------------------------------------------------------------------
            if self.train_params['save_checkpoints']:
                self.save_checkpoint(self.train_params['checkpoint_prefix'], ep)

        # Close the file writers
        self.writer_train.close()
        self.writer_val.close()
        # self.writer_grad.close()
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May  2 21:36:37 2018

@author: samitha herath
"""

from __future__ import division
import tensorflow as tf




def gaussian_kl(cov0, cov1, mean0, mean1):

    # Computes the KL-divergence between the features set 0 and set 1 assuming that the distributions are gaussian
    with tf.name_scope('compute_kl_div'):

        dm_10_ = tf.expand_dims(tf.subtract(mean1, mean0), axis=0)
        inv_cov1 = tf.matrix_inverse(cov1)
        d = tf.cast(tf.shape(cov0)[0], tf.float32)
        
        with tf.name_scope('ln_det_cov0_computation'):
            ln_det_cov0 = tf.linalg.logdet(cov0)

        with tf.name_scope('ln_det_cov1_computation'):
            ln_det_cov1 = tf.linalg.logdet(cov1)

        temp_mult = tf.matmul(dm_10_, inv_cov1)
        dkl_01_list = [tf.trace(tf.matmul(inv_cov1, cov0)),
                       tf.squeeze(tf.matmul(temp_mult, dm_10_, transpose_b=True)),
                       tf.scalar_mul(-1.0, d),
                       ln_det_cov1,
                       tf.scalar_mul(-1.0, ln_det_cov0)]

        dkl_01 = tf.scalar_mul(0.5, tf.add_n(dkl_01_list))

        sanity_check = 1.0

        return dkl_01, sanity_check


def sym_gaussian_kl(cov0, cov1, mean0, mean1, scope='symmetrized_kl'):

    with tf.name_scope(scope):

        dkl_01, sanity_01 = gaussian_kl(cov0, cov1, mean0, mean1)
        dkl_10, sanity_10 = gaussian_kl(cov1, cov0, mean1, mean0)

        sanity = 1

        return tf.scalar_mul(0.5, tf.add(dkl_01, dkl_10)), sanity


def coral_loss_function(cov0, cov1):

    with tf.name_scope('compute_cov_loss'):

        d_ = tf.cast(tf.shape(cov0)[0], tf.float32)
        normalizing_constant = tf.divide(1.0, tf.multiply(4.0, tf.square(d_)))
        cov_loss = tf.reduce_sum(tf.square(tf.subtract(cov0, cov1)))
        coral_loss = tf.multiply(normalizing_constant, cov_loss)

        sanity_check = 1

        return coral_loss, sanity_check

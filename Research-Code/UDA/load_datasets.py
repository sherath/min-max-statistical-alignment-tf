import cPickle as pkl
import os
import numpy as np
import scipy
import sys

from scipy.io import loadmat
from utils import u2t, s2t


class Mnist(object):
    def __init__(self, datadir=None):
        """MNIST domain train/test data
        """
        print "Loading MNIST"
        train = loadmat(os.path.join(datadir, 'mnist32_train.mat'))
        test = loadmat(os.path.join(datadir, 'mnist32_test.mat'))

        trainx = s2t(train['X'])
        trainy = train['y'].reshape(-1)
        trainy = np.eye(10)[trainy].astype('float32')

        testx = s2t(test['X'])
        testy = test['y'].reshape(-1)
        testy = np.eye(10)[testy].astype('float32')

        trainx = trainx.reshape(-1, 32, 32, 3).astype('float32')
        testx = testx.reshape(-1, 32, 32, 3).astype('float32')

        #----------------------------------------------------------------------
        # Samitha changed the following lines
        #----------------------------------------------------------------------
        self.train_set = {'x':trainx, 'y':trainy}
        self.test_set  = {'x':testx, 'y':testy}


class Mnistm(object):
    def __init__(self, shape=(28, 28, 3), seed=0, npc=None, datadir=None):
        """Mnist-M domain train/test data

        shape - (3,) HWC info
        """
        raise NotImplementedError('Did not change mnistm yet')
        print "Loading MNIST-M"
        data = pkl.load(open(os.path.join(datadir, 'mnistm_data.pkl')))
        labels = pkl.load(open(os.path.join(datadir, 'mnistm_labels.pkl')))

        trainx, trainy = data['train'], labels['train']
        validx, validy = data['valid'], labels['valid']
        testx, testy = data['test'], labels['test']
        trainx = np.concatenate((trainx, validx), axis=0)
        trainy = np.concatenate((trainy, validy), axis=0)

        trainx = self.resize_cast(trainx, shape)
        testx = self.resize_cast(testx, shape)

        #----------------------------------------------------------------------
        # Samitha changed the following lines
        #----------------------------------------------------------------------
        self.train_set = {'x':trainx, 'y':trainy}
        self.test_set  = {'x':testx, 'y':testy}

    @staticmethod
    def resize_cast(x, shape):
        H, W, C = shape
        x = x.reshape(-1, 28, 28, 3)

        resized_x = np.empty((len(x), H, W, 3), dtype='float32')
        for i, img in enumerate(x):
            # imresize returns uint8
            resized_x[i] = u2t(scipy.misc.imresize(img, (H, W)))

        return resized_x

class Svhn(object):
    def __init__(self, train='train', datadir=None):
        """SVHN domain train/test data

        train - (str) flag for using 'train' or 'extra' data
        """
        print "Loading SVHN"
        train = loadmat(os.path.join(datadir, '{:s}_32x32.mat'.format(train)))
        test = loadmat(os.path.join(datadir, 'test_32x32.mat'))

        # Change format
        trainx, trainy = self.change_format(train)
        testx, testy = self.change_format(test)

        #----------------------------------------------------------------------
        # Samitha changed the following lines
        #----------------------------------------------------------------------
        trainx_    = u2t(trainx)
        testx_     = u2t(testx)
        self.train_set = {'x':trainx_, 'y':trainy}
        self.test_set  = {'x':testx_, 'y':testy}

    @staticmethod
    def change_format(mat):
        """Convert X: (HWCN) -> (NHWC) and Y: [1,...,10] -> one-hot
        """
        x = mat['X'].transpose((3, 0, 1, 2))
        y = mat['y'].reshape(-1)
        y[y == 10] = 0
        y = np.eye(10)[y]
        return x, y

class SynDigits(object):
    def __init__(self, datadir=None):
        """Synthetic SVHN domain train/test data
        """
        print "Loading SynDigits"
        train = loadmat(os.path.join(datadir, 'synth_train_32x32.mat'))
        test = loadmat(os.path.join(datadir, 'synth_test_32x32.mat'))

        # Change format
        trainx, trainy = self.change_format(train)
        testx, testy = self.change_format(test)

        trainx_    = u2t(trainx)
        testx_     = u2t(testx)

        #----------------------------------------------------------------------
        # Samitha changed the following lines
        #----------------------------------------------------------------------        
        self.train_set = {'x':trainx_, 'y':trainy}
        self.test_set  = {'x':testx_, 'y':testy}


    @staticmethod
    def change_format(mat):
        """Convert X: (HWCN) -> (NHWC) and Y: [0,...,9] -> one-hot
        """
        x = mat['X'].transpose((3, 0, 1, 2))
        y = mat['y'].reshape(-1)
        y = np.eye(10)[y]
        return x, y

class Gtsrb(object):
    def __init__(self, datadir=None):
        """GTSRB street sign train/test adta
        """
        print "Loading GTSRB"
        data = loadmat(os.path.join(datadir, 'gtsrb.mat'))

        # Not really sure what happened here
        data['y'] = data['y'].reshape(-1)

        # Convert to one-hot
        n_class = data['y'].max() + 1
        data['y'] = np.eye(n_class)[data['y']]

        # Create train/test split
        np.random.seed(0) # TODO : Samitha changed this code here to fix the numpy seed
        # Following Asymmetric Tri-training protocol
        # https://arxiv.org/pdf/1702.08400.pdf
        shuffle = np.random.permutation(len(data['X']))

        x = data['X'][shuffle]
        y = data['y'][shuffle]
        n = 31367
        trainx, trainy = x[:n], y[:n]
        testx, testy = x[n:], y[n:]

        #----------------------------------------------------------------------
        # Samitha changed the following lines
        #----------------------------------------------------------------------
        trainx_    = u2t(trainx)
        testx_     = u2t(testx)
        self.train_set = {'x':trainx_, 'y':trainy}
        self.test_set  = {'x':testx_, 'y':testy}

class SynSigns(object):
    def __init__(self, datadir=None):
        """Synthetic street signs domain train/test data
        """
        print "Loading SynSigns"
        data = loadmat(os.path.join(datadir, 'synsigns.mat'))

        # Not really sure what happened here
        data['y'] = data['y'].reshape(-1)

        # Convert to one-hot
        n_class = data['y'].max() + 1
        data['y'] = np.eye(n_class)[data['y']]

        # Create train/test split
        np.random.seed(0) # TODO : Samitha changed this code here to fix the numpy seed
        shuffle = np.random.permutation(len(data['X']))

        x = data['X'][shuffle]
        y = data['y'][shuffle]
        n = 95000
        trainx, trainy = x[:n], y[:n]
        testx, testy = x[n:], y[n:]

        #----------------------------------------------------------------------
        # Samitha changed the following lines
        #----------------------------------------------------------------------
        trainx_    = u2t(trainx)
        testx_     = u2t(testx)
        self.train_set = {'x':trainx_, 'y':trainy}
        self.test_set  = {'x':testx_, 'y':testy}

class Cifar(object):
    def __init__(self, datadir=None):
        """CIFAR-10 modified domain train/test data

        Modification: one of the classes was removed to match STL
        """
        print "Loading CIFAR"
        train = loadmat(os.path.join(datadir, 'cifar_train.mat'))
        test = loadmat(os.path.join(datadir, 'cifar_test.mat'))

        # Get data
        trainx, trainy = train['X'], train['y']
        testx, testy = test['X'], test['y']

        # Convert to one-hot
        trainy = np.eye(9)[trainy.reshape(-1)]
        testy = np.eye(9)[testy.reshape(-1)]

        #----------------------------------------------------------------------
        # Samitha changed the following lines
        #----------------------------------------------------------------------
        trainx_    = u2t(trainx)
        testx_     = u2t(testx)
        self.train_set = {'x':trainx_, 'y':trainy}
        self.test_set  = {'x':testx_, 'y':testy}

class Stl(object):
    def __init__(self, datadir=None):
        """STL-10 modified domain train/test data

        Modification: one of the classes was removed to match CIFAR
        """
        print "Loading STL"
        sys.stdout.flush()
        train = loadmat(os.path.join(datadir, 'stl_train.mat'))
        test = loadmat(os.path.join(datadir, 'stl_test.mat'))

        # Get data
        trainx, trainy = train['X'], train['y']
        testx, testy = test['X'], test['y']

        # Convert to one-hot
        trainy = np.eye(9)[trainy.reshape(-1)]
        testy = np.eye(9)[testy.reshape(-1)]

        #----------------------------------------------------------------------
        # Samitha changed the following lines
        #----------------------------------------------------------------------
        trainx_    = u2t(trainx)
        testx_     = u2t(testx)
        self.train_set = {'x':trainx_, 'y':trainy}
        self.test_set  = {'x':testx_, 'y':testy}


def get_data(domain_id, datadir):
    """Returns Domain object based on domain_id
    """
    if domain_id == 'svhn':
        return Svhn(datadir=datadir)

    elif domain_id == 'digit':
        return SynDigits(datadir=datadir)

    elif domain_id == 'mnist':
        return Mnist(datadir=datadir)

    elif domain_id == 'mnistm':
        return Mnistm(shape=(32, 32, 3),datadir=datadir)

    elif domain_id == 'gtsrb':
        return Gtsrb(datadir=datadir)

    elif domain_id == 'sign':
        return SynSigns(datadir=datadir)

    elif domain_id == 'cifar':
        return Cifar(datadir=datadir)

    elif domain_id == 'stl':
        return Stl(datadir=datadir)

    else:
        raise Exception('dataset {:s} not recognized'.format(domain_id))

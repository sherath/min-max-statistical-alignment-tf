#!/usr/bin/env python2
# -*- coding: utf-8 -*-
'''
The ThreeStream Class contains methods for 
1. Training
2. Evaluating 
3. get_batch methods and images_preprocess methods for the Still/Frame images, Fused Stream and the Adverserial Classifier
'''


from __future__ import division
import numpy as np
import random
import nn_models
import losses as losses
import Train_and_Eval
from samv2.sam import sam

import tensorflow as tf
from tensorflow.contrib.framework import arg_scope
slim = tf.contrib.slim

IMAGE_HEIGHT = 32
IMAGE_WIDTH = 32


class Siamese(Train_and_Eval.Train_Eval):

    def __init__(self,
                 sess,
                 src_data,
                 trg_data,
                 SETUP_PARAMS):

        # ---------------------------------------------------------------------
        # Placeholders init
        # ---------------------------------------------------------------------
        self.src_x = None
        self.trg_x = None
        self.image_labels = None

        self.alpha_sam = None
        self.alpha_t_cls = None

        self.gamma_sam = None
        self.gamma_s_cls = None
        self.gamma_t_cls = None

        self.source_logits = None
        self.source_feats = None
        self.aligned_source_feats = None

        self.target_logits = None
        self.target_feats = None
        self.aligned_target_feats = None

        self.target_logits_val = None
        self.target_feats_val = None
        self.aligned_target_feats_val = None

        # -------------------------------------------------------------
        # SAM parameters
        # -------------------------------------------------------------
        self.stat_align_loss = None
        self.update_moments = None
        self.sam_sanity_check = None
        self.scaled_stat_align_loss = None

        # -------------------------------------------------------------
        # Losses
        # -------------------------------------------------------------
        self.source_train_loss = None
        self.target_train_loss = None
        self.sanity_target_train_loss = None
        self.target_train_loss_weighted = None
        self.target_val_loss = None
        self.base_net_train_loss = None

        # -------------------------------------------------------------
        # Accuracies
        # -------------------------------------------------------------
        self.source_train_acc = None
        self.target_val_acc = None

        # -------------------------------------------------------------
        # Variable sets and savers
        # -------------------------------------------------------------
        self.basenet_variables = None
        self.sam_variables = None
        self.full_model_saver = None

        # fix the random seed for the experiment-------------------------------
        tf.set_random_seed(0)
        np.random.seed(0)
        random.seed(0)
        # ---------------------------------------------------------------------

        # Methods passed in :
        # network_function  : the network structure build with tf.slim package     

        # Training and session
        self.sess = sess
        self.train_params = SETUP_PARAMS['train_params']

        # Network model functions
        self.network_function = getattr(nn_models, self.train_params['classifier_model'])
        self.network_arg_scope = getattr(nn_models, 'classifier_arg_scope')

        self.confusion_function = getattr(nn_models, self.train_params['confusion_model'])
        self.confusion_arg_scope = getattr(nn_models, 'confusion_model_arg_scope')

        # Dataset functions
        self.src_data = src_data
        self.trg_data = trg_data

        self.model_train_losses()

    def model_train_losses(self):

        image_dims = [IMAGE_HEIGHT, IMAGE_WIDTH, 3]

        # ---------------------------------------------------------------------
        # Create a placeholder for the network inputs
        # ---------------------------------------------------------------------
        self.src_x = tf.placeholder(tf.float32, [None] + image_dims, name='src_x')
        self.trg_x = tf.placeholder(tf.float32, [None] + image_dims, name='trg_x')
        self.image_labels = tf.placeholder(tf.int32, [None], name='image_labels')
        # ----------------------------------------------------------------------

        # ---------------------------------------------------------------------
        # alpha weights for the losses
        # ---------------------------------------------------------------------
        with tf.name_scope('alpha_loss_weights'):

            self.alpha_sam = tf.constant(self.train_params['alpha_weight_sam'])  # Weight on the sam alignment
            self.alpha_t_cls = tf.constant(self.train_params['alpha_weight_t_cls'])  # Weight on the kl loss

        # ----------------------------------------------------------------------
        # gamma swithces for alternating between losses 
        # ----------------------------------------------------------------------
        with tf.name_scope('gamma_loss_switches'):

            self.gamma_sam = tf.placeholder(tf.float32, shape=(), name='gamma_sam')  # Weight on the overall adverserial statistic loss for the basenetwork training (for the first 100 epochs it is 0, then switched up)
            self.gamma_s_cls = tf.placeholder(tf.float32, shape=(), name='gamma_s_cls')  # Weight on the overall direct statistic loss for the basenetwork training (for the first 100 epochs it is 0, then switched up)
            self.gamma_t_cls = tf.placeholder(tf.float32, shape=(), name='gamma_t_cls')  # Weight on the target discriminator

        # ---------------------------------------------------------------------
        # Base Network
        # ---------------------------------------------------------------------
        with tf.variable_scope('base_network') as sc_base:
            with slim.arg_scope(self.network_arg_scope()):

                # -------------------------------------------------------------
                # Training models
                # -------------------------------------------------------------
                self.source_logits, self.source_feats =\
                    self.network_function(inputs=self.src_x,
                                          num_classes=self.train_params['num_classes'],
                                          is_training=True,
                                          use_instance_norm=self.train_params['use_instance_norm'])

                if self.train_params['use_compact_feats']:
                    self.aligned_source_feats = tf.nn.tanh(self.source_feats)
                else:
                    self.aligned_source_feats = tf.identity(self.source_feats)

                sc_base.reuse_variables()  # Reuse the sc_base scope for the target domain stream as well

                self.target_logits, self.target_feats =\
                    self.network_function(inputs=self.trg_x,
                                          num_classes=self.train_params['num_classes'],
                                          is_training=True,
                                          use_instance_norm=self.train_params['use_instance_norm'])
                
                if self.train_params['use_compact_feats']:
                    self.aligned_target_feats = tf.nn.tanh(self.target_feats)
                else:
                    self.aligned_target_feats = tf.identity(self.target_feats)

                # --------------------------------------------------------------
                # Validation models
                # --------------------------------------------------------------
                self.target_logits_val, self.target_feats_val =\
                    self.network_function(inputs=self.trg_x,
                                          num_classes=self.train_params['num_classes'],
                                          is_training=False,
                                          use_instance_norm=self.train_params['use_instance_norm'])

                if self.train_params['use_compact_feats']:
                    self.aligned_target_feats_val = tf.nn.tanh(self.target_feats_val)
                else:
                    self.aligned_target_feats_val = tf.identity(self.target_feats_val)

        # ---------------------------------------------------------------------
        # SAM : Statistical Alignment Module
        # ---------------------------------------------------------------------
        with tf.variable_scope('statistical_alignment_module'):
            with arg_scope([self.confusion_function], 
                           use_compact_feats=self.train_params['use_compact_feats'], 
                           confused_dim=self.train_params['confused_dim'],
                           num_confusion_layers=self.train_params['num_confusion_layers']):
                self.stat_align_loss, self.update_moments, self.sam_sanity_check = \
                    sam(input0=self.aligned_source_feats,
                        input1=self.aligned_target_feats,
                        squeeze_inputs=False,
                        stop_grad0=self.train_params['stop_grad_source'],
                        unit_normal0=False,
                        conf_network=self.confusion_function,
                        conf_net_out_dim=None,
                        share_conf_network=True,
                        use_accumulate_moments=self.train_params['sam_use_accumulate_moments'],
                        momentum=self.train_params['sam_momentum'],
                        use_diag=self.train_params['sam_use_diag'],
                        batch_size=self.train_params['train_batch_size'],
                        cov_reg=1e-5,
                        functionality=self.train_params['sam_functionality'],
                        scope='stat_align_module')

            self.scaled_stat_align_loss = tf.scalar_mul(self.alpha_sam, self.stat_align_loss)

        # ----------------------------------------------------------------------
        # Classification and Statistics Losses
        # ----------------------------------------------------------------------
        with tf.variable_scope('classification_losses'):

            # source domain classification loss
            self.source_train_loss =\
                losses.softmax_cross_entropy_with_logits(self.source_logits,
                                                         self.image_labels,
                                                         num_classes=self.train_params['num_classes'])
            
            # cross-entropy regularization for the target domain
            self.target_train_loss, self.sanity_target_train_loss = losses.cross_entropy_regularizer(self.target_logits)
            self.target_train_loss_weighted = tf.scalar_mul(self.alpha_t_cls,
                                                            self.target_train_loss)
            
            # target domain validation loss
            self.target_val_loss = losses.softmax_cross_entropy_with_logits(self.target_logits_val,
                                                                            self.image_labels,
                                                                            num_classes=self.train_params['num_classes'])

        with tf.variable_scope('training_losses'):
            
            # classification, direct/confused statistics alignment and target's cross entropy regularizer
            self.base_net_train_loss = tf.add_n([tf.scalar_mul(self.gamma_s_cls, self.source_train_loss),          # Source discriminative loss
                                                tf.scalar_mul(self.gamma_t_cls, self.target_train_loss_weighted),  # Target discriminative loss (Entropy regularizer)
                                                tf.scalar_mul(self.gamma_sam, self.scaled_stat_align_loss)])       # Adverserial feature matching loss (Confused feature matching loss)

        # ----------------------------------------------------------------------
        # Evaluation Metrics
        # ----------------------------------------------------------------------
        with tf.variable_scope('classification_accs'):
        
            # Still image accuracy
            self.source_train_acc = tf.reduce_mean(tf.cast(tf.equal(tf.to_int32(tf.argmax(self.source_logits, 1)), self.image_labels), tf.float32))
            self.target_val_acc = tf.reduce_mean(tf.cast(tf.equal(tf.to_int32(tf.argmax(self.target_logits_val, 1)), self.image_labels), tf.float32))

        # ----------------------------------------------------------------------
        # Model variable segmentation : segments the model variables
        # ----------------------------------------------------------------------
        full_var_list = [v for v in tf.global_variables()]  # All variables in the model
        vars_excluded_from_train = self.train_params['excluded_training_variables']  # moving_variance and moving_average are not updated by gradient descent
        vars_excluded_from_train_basenet = self.train_params['excluded_training_variables_for_basenet']  # The block1, block2, conv1 and the logits layer are excluded

        # ----------------------------------------------------------------------
        # Variables to train on the model
        # ----------------------------------------------------------------------
        self.basenet_variables = self.subset_load_variables(full_var_list, ['base_network'], vars_excluded_from_train_basenet)
        self.sam_variables = self.subset_load_variables(full_var_list, ['statistical_alignment_module'], vars_excluded_from_train)

        print('================== Full Variables List =======================')
        for ful_var in full_var_list:
            print(ful_var.op.name)
        print('==============================================================')

        print('================= Basenet Trained Variables ==================')
        for base_var in self.basenet_variables:
            print(base_var.op.name)
        print('==============================================================')
        
        print('  ')
        
        print('==================== SAM Trained Variables ===================')
        for tempvar in self.sam_variables:
            print(tempvar.op.name)
        print('==============================================================')

        print('  ')

        # Initialize the Saver operators for each stream   
        with tf.variable_scope('All_Savers_Restorers'):      
            self.full_model_saver = tf.train.Saver(name='Full_Model')

    # --------------------------------------------------------------------------
    # Initializing methods for the model    
    # --------------------------------------------------------------------------
    def subset_load_variables(self, full_var_list, include_keywords, exempt_keywords):

        included_variables = self.include_subset_load_variables(full_var_list, include_keywords)
        return self.exclude_subset_load_variables(included_variables, exempt_keywords)

    # Include a subset of variables to be loaded
    def include_subset_load_variables(self, full_var_list, include_keywords):

        include_vars = list()
        for v in full_var_list:
            if any(include_keyword in v.op.name for include_keyword in include_keywords):
                include_vars.append(v)

        return include_vars

    # Exempt a subset of variables to be loaded
    def exclude_subset_load_variables(self, full_var_list, exempt_keywords):
           
           exempt_vars = list()
           for v in full_var_list:
               if any(exempt_keyword in v.op.name for exempt_keyword in exempt_keywords):
                   exempt_vars.append(v)
           
           return list(set(full_var_list) -set(exempt_vars))

    def save_checkpoint(self, checkpoint_prefix, checkpoint_index):           
        
           checkpoint_save_path = self.train_params['checkpoint_dir'] + '/' + checkpoint_prefix + '-' + str(checkpoint_index) + '.ckpt'
           self.full_model_saver.save(self.sess, checkpoint_save_path)
           print('checkpoint saved : ' + checkpoint_save_path)
          

 

   


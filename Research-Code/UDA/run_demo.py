#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Run this code to train and eval the network
"""

import os
import sys
import argparse
import tensorflow as tf
import Siamese
from load_datasets import get_data

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


def main(_):

    # ==========================================================================
    # Argument collector
    # ==========================================================================

    parser = argparse.ArgumentParser(description='Input arguments to get different training models',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # --------------------------------------------------------------------------
    # Experiment Parameters
    # --------------------------------------------------------------------------
    parser.add_argument('--exp_title', type=str, default='small_samv2_min_max')
    parser.add_argument('--exp_id', type=int, default=1)

    parser.add_argument('--dataset_folder', type=str, default='/flush3/her171/Data/Small')
    parser.add_argument('--source_name', type=str, default='mnist')
    parser.add_argument('--target_name', type=str, default='svhn')

    # --------------------------------------------------------------------------
    # SAM parameters
    # --------------------------------------------------------------------------
    parser.add_argument('--sam_use_accumulate_moments', action='store_true')
    parser.add_argument('--sam_momentum', type=float, default=0.5)  # Accumulation momentum of statistics
    parser.add_argument('--sam_use_diag', action='store_true')  # Use diagonal covariance matrix
    parser.add_argument('--sam_functionality', type=str, default='min_max')  # 'min', 'min_max' or 'coral'
    parser.add_argument('--stop_grad_source', action='store_true')  # Stop back propagating on source alignment
    parser.add_argument('--gen_train_delay', type=int, default=0)
    parser.add_argument('--use_compact_feats', action='store_true')

    # --------------------------------------------------------------------------
    # Loss parameters
    # --------------------------------------------------------------------------
    parser.add_argument('--alpha_weight_sam', type=float, default=0.01)  # KL loss weight confised(adverserial) Reduce this by 0.1
    parser.add_argument('--alpha_weight_t_cls', type=float, default=0.0)  # Weight for the target domain classifier

    # --------------------------------------------------------------------------
    # Training Parameters
    # --------------------------------------------------------------------------
    parser.add_argument('--solver_type', type=str, default='rmsp')
    parser.add_argument('--learning_rate', type=float, default=0.001)
    parser.add_argument('--weight_decay_base_network', type=float, default=0.0001)

    parser.add_argument('--train_batch_size', type=int, default=256)
    parser.add_argument('--val_batch_size', type=int, default=256)
    parser.add_argument('--number_of_epochs', type=int, default=2000)
    parser.add_argument('--display_interval', type=int, default=100)

    # --------------------------------------------------------------------------
    # Model Parameters
    # --------------------------------------------------------------------------
    parser.add_argument('--classifier_model', type=str, default='classifier_function_mid_feats')
    parser.add_argument('--confusion_model_id', type=int, default=4)
    parser.add_argument('--confused_dim', type=int, default=64)
    parser.add_argument('--num_confusion_layers', type=int, default=4)    
    parser.add_argument('--use_instance_norm', action='store_true')

    # --------------------------------------------------------------------------
    # Training and Loading Variables
    # --------------------------------------------------------------------------
    parser.add_argument('--excluded_loading_variables', type=str, default="['Adam','Momentum']")
    parser.add_argument('--excluded_training_variables', type=str, default="['Adam','Momentum','moving_mean','moving_variance','moving_mean_vec','moving_cov_mat']")
    parser.add_argument('--excluded_training_variables_for_basenet', type=str, default="['Adam','Momentum','moving_mean','moving_variance']")
    parser.add_argument('--base_network_update_op_scope', type=str, default="base_network/cnn_1")
    parser.add_argument('--sam_update_op_scope', type=str, default="statistical_alignment_module/stat_align_module")

    # Checkpoint/Summaries saving details
    parser.add_argument('--save_checkpoints', type=bool, default=True)
    parser.add_argument('--log_dir', type=str, default='/flush3/her171/Interactive-CVPR19/Domain-Adaptation/SAM-Small')
    parser.add_argument('--checkpoint_prefix', type=str, default='domain-conf-large-net')

    # Running the debugger
    parser.add_argument('--debug_mode', action='store_true')
    parser.add_argument('--checked_sanity', type=str, default='None')  # use None to stop sanity

    argument_inputs = parser.parse_args()

    # =========================================================================
    # Parsing the input arguments
    # =========================================================================
    SETUP_PARAMS = {}  # Main dictionary to save the setup parameters
    TRAIN_PARAMS = {}  # Training parameter dictionary

    num_classes_dict = {'mnist': 10, 'mnistm': 10, 'digit': 10, 'svhn': 10, 'cifar': 9, 'stl': 9, 'sign': 43}
    TRAIN_PARAMS['num_classes'] = num_classes_dict[argument_inputs.source_name]

    # --------------------------------------------------------------------------
    # Model parameters
    # --------------------------------------------------------------------------
    TRAIN_PARAMS['confusion_model'] = 'confusion_function_' + str(argument_inputs.confusion_model_id)
    TRAIN_PARAMS['confused_dim'] = argument_inputs.confused_dim
    TRAIN_PARAMS['num_confusion_layers'] = argument_inputs.num_confusion_layers    
    TRAIN_PARAMS['classifier_model'] = argument_inputs.classifier_model
    TRAIN_PARAMS['use_instance_norm'] = argument_inputs.use_instance_norm

    # --------------------------------------------------------------------------
    # SAM parameters
    # --------------------------------------------------------------------------
    TRAIN_PARAMS['sam_use_accumulate_moments'] = argument_inputs.sam_use_accumulate_moments
    TRAIN_PARAMS['sam_momentum'] = argument_inputs.sam_momentum
    TRAIN_PARAMS['sam_use_diag'] = argument_inputs.sam_use_diag
    TRAIN_PARAMS['sam_functionality'] = argument_inputs.sam_functionality
    TRAIN_PARAMS['stop_grad_source'] = argument_inputs.stop_grad_source
    TRAIN_PARAMS['gen_train_delay'] = argument_inputs.gen_train_delay
    TRAIN_PARAMS['use_compact_feats'] = argument_inputs.use_compact_feats

    # --------------------------------------------------------------------------
    # Training Parameters
    # --------------------------------------------------------------------------
    TRAIN_PARAMS['alpha_weight_sam'] = argument_inputs.alpha_weight_sam
    TRAIN_PARAMS['alpha_weight_t_cls'] = argument_inputs.alpha_weight_t_cls

    TRAIN_PARAMS['excluded_loading_variables'] = eval(argument_inputs.excluded_loading_variables)
    TRAIN_PARAMS['excluded_training_variables'] = eval(argument_inputs.excluded_training_variables)
    TRAIN_PARAMS['excluded_training_variables_for_basenet'] = eval(argument_inputs.excluded_training_variables_for_basenet)
    TRAIN_PARAMS['base_network_update_op_scope'] = argument_inputs.base_network_update_op_scope
    TRAIN_PARAMS['sam_update_op_scope'] = argument_inputs.sam_update_op_scope
    
    TRAIN_PARAMS['solver_type'] = argument_inputs.solver_type
    TRAIN_PARAMS['learning_rate'] = argument_inputs.learning_rate
    TRAIN_PARAMS['weight_decay_base_network'] = argument_inputs.weight_decay_base_network 
        
    TRAIN_PARAMS['train_batch_size'] = argument_inputs.train_batch_size
    TRAIN_PARAMS['number_of_epochs'] = argument_inputs.number_of_epochs

    # Logging and evaluation parameters
    domain_set = argument_inputs.source_name + '_' + argument_inputs.target_name
    full_exp_title = domain_set + '_' + argument_inputs.exp_title + '_' + str(argument_inputs.exp_id)
    
    TRAIN_PARAMS['summaries_dir'] = argument_inputs.log_dir + '/summaries/' + full_exp_title
    
    if not os.path.exists(TRAIN_PARAMS['summaries_dir']):
        os.makedirs(TRAIN_PARAMS['summaries_dir']+'/train')   
        os.makedirs(TRAIN_PARAMS['summaries_dir']+'/val')  
        # os.makedirs(TRAIN_PARAMS['summaries_dir']+'/grad')
    
    TRAIN_PARAMS['checkpoint_dir'] = argument_inputs.log_dir + '/checkpoints/' + full_exp_title
    
    if not os.path.exists(TRAIN_PARAMS['checkpoint_dir']):
        os.makedirs(TRAIN_PARAMS['checkpoint_dir'])   

    TRAIN_PARAMS['save_checkpoints'] = argument_inputs.save_checkpoints
    TRAIN_PARAMS['checkpoint_prefix'] = argument_inputs.checkpoint_prefix
    TRAIN_PARAMS['val_batch_size'] = argument_inputs.val_batch_size
    TRAIN_PARAMS['display_interval'] = argument_inputs.display_interval

    # Run the debugger
    TRAIN_PARAMS['debug_mode'] = argument_inputs.debug_mode
    TRAIN_PARAMS['checked_sanity'] = argument_inputs.checked_sanity

    SETUP_PARAMS['train_params'] = TRAIN_PARAMS
    
    # ==========================================================================
    # Train and Eval the model
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Grab data from the datasets
    # --------------------------------------------------------------------------
    src_data = get_data(argument_inputs.source_name, argument_inputs.dataset_folder)  # Returns an object for source
    trg_data = get_data(argument_inputs.target_name, argument_inputs.dataset_folder)  # Returns an object for target

    # --------------------------------------------------------------------------
    # Run the code
    # --------------------------------------------------------------------------
    run_config = tf.ConfigProto()
    run_config.gpu_options.allow_growth = True
    run_config.gpu_options.per_process_gpu_memory_fraction = 1

    print(SETUP_PARAMS['train_params'])
    tf.set_random_seed(0)

    with tf.Session(config=run_config) as sess:
        
        siamese = Siamese.Siamese(sess,
                                  src_data=src_data,
                                  trg_data=trg_data,
                                  SETUP_PARAMS=SETUP_PARAMS)

    # call the train_eval method to train and evaluate the model
    siamese.train_eval()


if __name__ == '__main__':
    tf.app.run()

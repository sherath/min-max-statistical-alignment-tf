#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 13:43:45 2018

@author: her171
"""

from __future__ import division
import numpy as np

import tensorflow as tf
slim = tf.contrib.slim

#----------------------------------------------------------------------
# Some loss function definitions
#----------------------------------------------------------------------

def gaussian_kl(features_0, features_1, batch_size=64, reg_weight = 0.01):
    # Computes the KL-divergence between the features set 0 and set 1 assuming that the distributions are gaussian
    
    with tf.name_scope('gather_features_for_gaussian_kl'):
        squeezed_features_0 = tf.squeeze(features_0, axis=[1, 2])
        squeezed_features_1 = tf.squeeze(features_1, axis=[1, 2])
        
        N = batch_size
        d = tf.cast(tf.shape(squeezed_features_0)[1], tf.float32)
        d_= tf.shape(squeezed_features_0)[1]
        
        features_0_mean_vec = tf.reduce_mean(squeezed_features_0, axis=0)
        features_1_mean_vec = tf.reduce_mean(squeezed_features_1, axis=0)
        
        features_0_mean  = tf.tile(tf.expand_dims(features_0_mean_vec, axis=0), [N, 1])
        features_1_mean  = tf.tile(tf.expand_dims(features_1_mean_vec, axis=0), [N, 1])
        
        centered_features_0 = tf.subtract(squeezed_features_0, features_0_mean)
        centered_features_1 = tf.subtract(squeezed_features_1, features_1_mean)
  
    
    with tf.name_scope('compute_covariance_for_gaussian_kl'):
        
        features_0_cov = tf.scalar_mul(np.float32(1/(N-1)), tf.matmul(centered_features_0, centered_features_0, transpose_a=True))
        features_1_cov = tf.scalar_mul(np.float32(1/(N-1)), tf.matmul(centered_features_1, centered_features_1, transpose_a=True))            
    
    
    with tf.name_scope('compute_kl_div'):
        
        dm_10_   = tf.expand_dims(tf.subtract(features_1_mean_vec, features_0_mean_vec), axis=0)
                        
        reg_cov0 = tf.add(features_0_cov, tf.scalar_mul(reg_weight, tf.eye(d_, dtype=tf.float32)))
        reg_cov1 = tf.add(features_1_cov, tf.scalar_mul(reg_weight, tf.eye(d_, dtype=tf.float32)))
            
        inv_cov1 = tf.matrix_inverse(reg_cov1)
        
        with tf.name_scope('ln_det_cov0_computation'):
            # cov0_eig, _ = tf.linalg.eigh(reg_cov0)
            # ln_det_cov0 = tf.reduce_sum(tf.log(cov0_eig))
            ln_det_cov0 = tf.linalg.logdet(reg_cov0)
            
        with tf.name_scope('ln_det_cov1_computation'):
            # cov1_eig, _ = tf.linalg.eigh(reg_cov1)
            # ln_det_cov1 = tf.reduce_sum(tf.log(cov1_eig))
            ln_det_cov1 = tf.linalg.logdet(reg_cov1)
            

        #--------------------------------------------------------------
            


        temp_mult = tf.matmul(dm_10_, inv_cov1)
        
        dkl_01_list =  [tf.trace(tf.matmul(inv_cov1, reg_cov0)),
                        tf.squeeze(tf.matmul(temp_mult, dm_10_, transpose_b=True)),
                        tf.scalar_mul(-1.0, d),
                        ln_det_cov1,
                        tf.scalar_mul(-1.0, ln_det_cov0)]            
        

        dkl_01 =  tf.scalar_mul(0.5, tf.add_n(dkl_01_list))
                                               
        sanity_check =  1.0
        
        return dkl_01, sanity_check



def sym_gaussian_kl(features_0, features_1, batch_size=64, reg_weight = 0.01):
    with tf.name_scope('symmetric_kl_divergence'):
        
        dkl_01, sanity_01 = gaussian_kl(features_0, features_1, batch_size=batch_size, reg_weight = reg_weight)
        dkl_10, sanity_10 = gaussian_kl(features_1, features_0, batch_size=batch_size, reg_weight = reg_weight)
        
        sanity = 1
        
        return tf.add(dkl_01, dkl_10), sanity


def covariance_loss_function(source_features, target_features, reg_weight = 0.01, loss_type='frob', batch_size=64):

    with tf.name_scope('gather_features_for_cov_loss'):
        
        squeezed_source_feats = tf.squeeze(source_features, axis=[1, 2])
        squeezed_target_feats = tf.squeeze(target_features, axis=[1, 2])
        
        N = batch_size
        # N = tf.shape(squeezed_source_feats)[0]
        d = tf.shape(squeezed_source_feats)[1]
        
        source_mean  = tf.tile(tf.expand_dims(tf.reduce_mean(squeezed_source_feats, axis=0), axis=0),[N, 1])
        target_mean  = tf.tile(tf.expand_dims(tf.reduce_mean(squeezed_target_feats, axis=0), axis=0),[N, 1])
        
        centered_source_feats = tf.subtract(squeezed_source_feats, source_mean)
        centered_target_feats = tf.subtract(squeezed_target_feats, target_mean)
  
    
    with tf.name_scope('compute_covariance'):
        
        source_cov = tf.scalar_mul(np.float32(1/(N-1)), tf.matmul(centered_source_feats, centered_source_feats, transpose_a=True))
        target_cov = tf.scalar_mul(np.float32(1/(N-1)), tf.matmul(centered_target_feats, centered_target_feats, transpose_a=True))

        reg_source_cov = tf.add(source_cov, tf.scalar_mul(reg_weight, tf.eye(d)))
        reg_target_cov = tf.add(target_cov, tf.scalar_mul(reg_weight, tf.eye(d)))


    with tf.name_scope('compute_cov_loss'):

        # Select a specific loss type ---------------------------------
        if loss_type == 'frob':  
             cov_loss = tf.norm(tf.subtract(reg_source_cov, reg_target_cov), ord = 'fro',axis=(0,1))
            
        elif loss_type == 'stein':
            
            cov_addition = tf.scalar_mul(0.5, tf.add(reg_source_cov, reg_target_cov))
            cov_mult     = tf.matmul(reg_source_cov, reg_target_cov)
            cov_loss     = tf.subtract(tf.linalg.logdet(cov_addition), tf.scalar_mul(0.5, tf.linalg.logdet(cov_mult)))
            
        sanity_check = 1
   
        #--------------------------------------------------------------
        
        return cov_loss, sanity_check

def mean_loss_function(source_features, target_features):

    with tf.name_scope('gather_features_for_mean_loss'):
        
        squeezed_source_feats = tf.squeeze(source_features, axis=[1, 2])
        squeezed_target_feats = tf.squeeze(target_features, axis=[1, 2])
        
        # N = tf.shape(squeezed_source_feats)[0]
        # d = tf.shape(squeezed_source_feats)[1]
        
        source_mean  = tf.reduce_mean(squeezed_source_feats, axis=0)
        target_mean  = tf.reduce_mean(squeezed_target_feats, axis=0)

    with tf.name_scope('compute_mean_loss'):

        mean_loss = tf.norm(tf.subtract(source_mean, target_mean), ord = 'euclidean')
        sanity_check = 1

        return mean_loss, sanity_check

def cross_entropy_regularizer(x):
    
    x_sm = tf.add(tf.nn.softmax(logits=x), np.finfo(np.float32).eps)
    log_x_sm = tf.log(x_sm)
    x_sm_log_x_sm = tf.reduce_sum(tf.multiply(x_sm, log_x_sm), axis=1)
    sanity = 1.0
    
    return tf.scalar_mul(-1.0, tf.reduce_mean(x_sm_log_x_sm)), sanity


def softmax_cross_entropy_with_logits(x, y, num_classes):
    one_hot_labels  = slim.one_hot_encoding(y, num_classes)
    return tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=x, labels=one_hot_labels))   
       
































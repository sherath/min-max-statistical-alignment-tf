from __future__ import division
import warnings
import tensorflow as tf
from tensorflow.contrib.framework import add_arg_scope
from tensorflow.contrib.framework import arg_scope
from align_losses import sym_gaussian_kl
from align_losses import coral_loss_function

slim = tf.contrib.slim

# ------------------------------------------------------------
# New layers for the module
# ------------------------------------------------------------


@add_arg_scope
def scale_gradient(x, scale, scope='scale_grad', reuse=None):

    # Gradient scale layer of Ganin etal.,
    with tf.name_scope(scope):
        output = (1 - scale) * tf.stop_gradient(x) + scale * x
    return output

# ------------------------------------------------------------
# Functions for accumulating the moments
# ------------------------------------------------------------


def _assign_moving_average_op(orig_val, new_val, momentum, name):
    # returns a new op with the moving average
    with tf.name_scope(name):
        scaled_diff = (1 - momentum) * (new_val - orig_val)
        return tf.add(orig_val, scaled_diff)


def _assign_moving_average_var(orig_val, new_val, momentum, name):
    with tf.name_scope(name):
        scaled_diff = (1 - momentum)*(new_val - orig_val)
        return tf.assign_add(orig_val, scaled_diff)


@add_arg_scope
def accumulate_moments_op(orig_cov,
                          orig_mean,
                          cov,
                          mean,
                          momentum=0.99,
                          scope='accumulate_moments_op'):

    # Accumulates the moments and returns an op.

    # Args :
    # orig_cov : moving covariance, the reference to the accumulation variable
    # orig_mean : moving mean, the reference to the accumulation variable

    # cov : batch covariance op
    # mean : batch mean op
    # momentum : the moving average momentum for accumulating the domain moments.
    #            Lower the momentum, faster the accumulated stats will change according to batch stats.

    # Returns :
    # new_cov : moving average op of the covariance matrix
    # new_mean : moving average op of the mean vector

    # Note : there are update ops in this function. Makesure the solver is correctly configured for these update ops

    with tf.name_scope(scope):

        new_cov = _assign_moving_average_op(orig_cov, cov, momentum, 'updated_cov_op')
        new_mean = _assign_moving_average_op(orig_mean, mean, momentum, 'updated_mean_op')

        return new_cov, new_mean


@add_arg_scope
def accumulate_moments_var(cov,
                           mean,
                           momentum=0.99,
                           scope='accumulate_moments_var'):

    # TODO : write some code to reset the moving moments to zero (i.e. initial values)
    # Accumulate the moments in the vars. And returns the update ops to accumulate the moments.
    # Important : the update ops should be run after the train step

    # Args :
    # cov : batch covariance op
    # mean : batch mean op
    # momentum : the moving average momentum for accumulating the domain moments.
    #            Lower the momentum, faster the accumulated stats will change according to batch stats.

    # Returns :
    # moving_cov : moving average of the covariance matrix
    # moving_mean : moving average of the mean vector
    # update_moments : updates the accumulated moments variables in the module.

    with tf.variable_scope(scope):

        # Get mean and variance, normalize input
        exemplar_identity_mat = tf.eye(cov.get_shape().as_list()[0], dtype="float32")
        # exemplar_zero_mat = tf.zeros_like(cov, dtype="float32")

        # TODO : Note the initializer for the moving_cov is a constant value. Hence, a shape shouldn't be passed
        # as an argument. Anyway, if the initializer is changed the shape should be defined.
        moving_cov = tf.get_variable('moving_cov_mat', initializer=exemplar_identity_mat, trainable=False)
        moving_mean = tf.get_variable('moving_mean_vec', mean.get_shape().as_list(), initializer=tf.zeros_initializer, trainable=False)

        update_cov = _assign_moving_average_var(moving_cov, cov, momentum, 'update_cov_var')
        update_mean = _assign_moving_average_var(moving_mean, mean, momentum, 'update_mean_var')

        update_moments = tf.group(update_cov, update_mean)

        # Deprecicated from manual updating mechanism with GraphKeys
        # tf.add_to_collection('update_ops', update_cov)
        # tf.add_to_collection('update_ops', update_mean)

        return moving_cov, moving_mean, update_moments


# ------------------------------------------------------------
# Moments and Losses computation
# ------------------------------------------------------------


@add_arg_scope
def compute_moments(x,
                    batch_size=64,
                    reg_weight=1e-7,
                    use_diag=False,
                    scope='compute_moments'):
    # Args :
    # x : input features op
    # batch_size : the input batch size. This should be constant during training time
    # use_diag : zero the non-diagonal values of the covariance matrix
    # scope : name scope

    # Return :
    # x_mean_vec : computed mean vector
    # x_cov_reg_ : covariance matrix with regularized diagonal

    with tf.name_scope(scope):

        with tf.name_scope('gather_features'):

            squeezed_x = tf.squeeze(x, axis=[1, 2])

            reg = tf.constant(reg_weight)

            n = tf.constant(batch_size)
            n_ = tf.cast(n, tf.float32)  # Float value for divisions

            d = tf.shape(squeezed_x)[1]
            # d_ = tf.cast(d, tf.float32)  # Float value for divisions

        with tf.name_scope('compute_moments'):

            # compute means
            x_mean_vec = tf.reduce_mean(squeezed_x, axis=0)
            centered_x = tf.subtract(squeezed_x, tf.expand_dims(x_mean_vec, axis=0))

            # compute covariance
            scaling_const = tf.divide(1.0, tf.subtract(n_, 1.0))
            x_cov = tf.scalar_mul(scaling_const,
                                  tf.matmul(centered_x, centered_x, transpose_a=True))

            # added this to the debug_branch for adaptive regularization
            x_cov_trace = tf.trace(x_cov)
            reg_ = tf.scalar_mul(reg, x_cov_trace)

            # regularize the covariance matrix diagonal for inverse computation
            x_cov_reg = tf.add(x_cov,
                               tf.scalar_mul(reg_, tf.eye(d, dtype=tf.float32)))

            # diagonalization flag
            if use_diag:
                x_cov_reg_ = tf.diag(tf.diag_part(x_cov_reg))
                warnings.warn("Using diagonalized covariance matrix", RuntimeWarning)
            else:
                x_cov_reg_ = tf.identity(x_cov_reg)

            # use this parameter as a sanity check input to the module
            moments_sanity = 1.0

            return x_mean_vec, x_cov_reg_, moments_sanity


# -----------------------------------------------------------------------------
# SAM function definition
# -----------------------------------------------------------------------------

@add_arg_scope
def sam(input0=None,
        input1=None,
        squeeze_inputs=False,
        stop_grad0=False,
        unit_normal0=False,
        conf_network=None,
        conf_net_out_dim=None,
        share_conf_network=True,
        use_accumulate_moments=False,
        momentum=0.99,
        use_diag=False,
        batch_size=None,
        cov_reg=1e-7,
        functionality='min_max',
        scope='stat_align_module'):

        # TODO : Implement a way to reset the accumulated moments if needed.
        #        This could be implemented with a placeholder.
        # TODO : modify sam to include constraints
        # TODO : Internally define conf_net_out_dim parameter with the input0 dimensionality

        # Args :
        # input0 : input0 a tensor with input features from domain0
        # input1 : input0 a tensor with input features from domain1
        # squeeze_inputs : if true this will squeeze the [1, 2] dimensions of the input tensors
        # stop_grad0 : will stop back propagating on the input0. Useful when only the domain1 features
        #              are to be learned such that the alignment is good.
        # unit_normal0 : use a unit normal distribution as confused moments of domain0.
        #                This is useful when the alignment module is trained as a whitening module
        # conf_network : a tf.slim network model for the confusion function
        # conf_net_out_dim : output feature dimensionality of the confusion model. This model could be changed.
        # share_conf_network : if True the confusion function parameters will be shared between 
        #                      domain0 and domain1
        # use_accumulate_moments : if True this will accumulate the batch mean vector and the
        #                          covariance matrix when the alignment is performed.
        #                          This idea is similar to batchnorm. To accumulate the moments
        #                          the update ops should be used as in batch normalization.
        # momentum : the moving average momentum for accumulating the domain moments.
        #            Lower the momentum, faster the accumulated stats will change according to batch stats.
        # use_diag : use diagonal covariance matrix (like in batch_norm)
        # batch_size : training batch size. This is required to be fixed during training
        # cov_reg : the additive regularizer for covariance matrices to make sure the diagonals are non-zero
        # functionality : 'min_max', 'min', 'coral'
        # scope : the scope of the stat align module

        # Returns :
        # An op that min-max KL alignment between input0, and input1 or unit gaussian and input1
        # update_moments : updates the accumulated moments variablces in the module.
        #                  If there weren't any accumulation flag set these ops will be blank

        # Update in v1_1 : the stop gradient in v1 (un-indexed version) is affecting the confusion model as well.
        # In this version the stop gradient option is brought before the confusion network. I.e., just after the inputs to the sam module.

        with tf.variable_scope(scope):

            # do not back propagate to domain 0 if the stop gradient option is used
            if stop_grad0:
                input0_ = tf.stop_gradient(input0)
            else:
                input0_ = tf.identity(input0)

            if squeeze_inputs:
                if not unit_normal0:
                    input0__ = tf.squeeze(input0_, axis=[1, 2])
                input1_ = tf.squeeze(input1, axis=[1, 2])
            else:
                if not unit_normal0:
                    input0__ = tf.identity(input0_)
                input1_ = tf.identity(input1)

            # if the module functionality is a a min-max
            if functionality == 'min_max':
                print('creating a min-max sam module')
                with tf.variable_scope('sam_network') as sam_net_scope:
                    with arg_scope([scale_gradient], scale=-1.0):

                        # input gradient reversal
                        if not unit_normal0:
                            scaled_input0_ = scale_gradient(input0__)
                        scaled_input1_ = scale_gradient(input1_)

                        if not unit_normal0:
                            conf_input0_ = conf_network(inputs=scaled_input0_,
                                                        is_training=True,
                                                        output_dim=conf_net_out_dim)

                            # use the same variable scope to share variables
                            if share_conf_network:
                                sam_net_scope.reuse_variables()

                        conf_input1_ = conf_network(inputs=scaled_input1_,
                                                    is_training=True,
                                                    output_dim=conf_net_out_dim)

                        # output gradient reversal
                        if not unit_normal0:
                            rev_input0_ = scale_gradient(conf_input0_)
                        rev_input1_ = scale_gradient(conf_input1_)

                with tf.variable_scope('sam_min_max_align'):
                    with arg_scope([compute_moments], batch_size=batch_size, use_diag=use_diag, reg_weight=cov_reg), \
                         arg_scope([accumulate_moments_var, accumulate_moments_op], momentum=momentum):

                            # compute the moments of ditributions
                            rev_input1_mean, rev_input1_cov, moments1_sanity = compute_moments(x=rev_input1_,
                                                                                               scope='compute_moments_1')
                            if unit_normal0:
                                rev_input0_mean = tf.zeros_like(rev_input1_mean)
                                rev_input0_cov = tf.eye(tf.shape(rev_input1_cov)[0], dtype=tf.float32)
                            else:
                                rev_input0_mean, rev_input0_cov, moments0_sanity = compute_moments(x=rev_input0_,
                                                                                                   scope='compute_moments_0')

                            # accumulate the moments if required
                            if use_accumulate_moments:

                                # --------------------------------------------------------------------------------------
                                # Accumulation ops and moving moments variables
                                # --------------------------------------------------------------------------------------
                                moving_cov_var0_, moving_mean_var0_, update_moments_0_ = \
                                    accumulate_moments_var(cov=rev_input0_cov,
                                                           mean=rev_input0_mean,
                                                           scope='accumulate_moments_var0')

                                moving_cov_var1_, moving_mean_var1_, update_moments_1_ = \
                                    accumulate_moments_var(cov=rev_input1_cov,
                                                           mean=rev_input1_mean,
                                                           scope='accumulate_moments_var1')

                                # --------------------------------------------------------------------------------------
                                # Moving moments ops
                                # --------------------------------------------------------------------------------------
                                rev_input0_cov_, rev_input0_mean_ = \
                                    accumulate_moments_op(orig_cov=moving_cov_var0_,
                                                          orig_mean=moving_mean_var0_,
                                                          cov=rev_input0_cov,
                                                          mean=rev_input0_mean,
                                                          scope='accumulate_moments_op0')

                                rev_input1_cov_, rev_input1_mean_ = \
                                    accumulate_moments_op(orig_cov=moving_cov_var1_,
                                                          orig_mean=moving_mean_var1_,
                                                          cov=rev_input1_cov,
                                                          mean=rev_input1_mean,
                                                          scope='accumulate_moments_op1')

                                update_moments = tf.group(update_moments_0_, update_moments_1_)

                            else:
                                rev_input0_mean_ = tf.identity(rev_input0_mean)
                                rev_input0_cov_ = tf.identity(rev_input0_cov)
                                rev_input1_mean_ = tf.identity(rev_input1_mean)
                                rev_input1_cov_ = tf.identity(rev_input1_cov)

                                update_moments = tf.no_op()

                            # samv1 stop_gradiet op was here
                            rev_input0_mean__ = tf.identity(rev_input0_mean_)
                            rev_input0_cov__ = tf.identity(rev_input0_cov_)

                            # compute kl-alignment loss
                            with tf.device("/cpu:0"):
                                align_loss, kl_sanity = sym_gaussian_kl(cov0=rev_input0_cov__,
                                                                        cov1=rev_input1_cov_,
                                                                        mean0=rev_input0_mean__,
                                                                        mean1=rev_input1_mean_,
                                                                        scope='min_max_kl_loss')

            # If not min_max (min and coral will be handled separately)
            else:
                with tf.variable_scope('sam_align'):
                    with arg_scope([compute_moments], batch_size=batch_size, use_diag=use_diag, reg_weight=cov_reg), \
                         arg_scope([accumulate_moments_var, accumulate_moments_op], momentum=momentum):

                        # compute the moments of distributions
                        input1_mean, input1_cov, moments1_sanity = compute_moments(x=input1_,
                                                                                   scope='compute_moments_1')
                        if unit_normal0:
                            input0_mean = tf.zeros_like(input1_mean)
                            input0_cov = tf.eye(tf.shape(input1_cov)[0], dtype=tf.float32)
                        else:
                            input0_mean, input0_cov, moments0_sanity = compute_moments(x=input0__,
                                                                                       scope='compute_moments_0')

                        # accumulate the moments if required
                        if use_accumulate_moments:

                            # ------------------------------------------------------------------------------------------
                            # Accumulation ops and moving moments variables
                            # ------------------------------------------------------------------------------------------
                            moving_cov_var0_, moving_mean_var0_, update_moments_0_ = \
                                accumulate_moments_var(cov=input0_cov,
                                                       mean=input0_mean,
                                                       scope='accumulate_moments_var0')

                            moving_cov_var1_, moving_mean_var1_, update_moments_1_ = \
                                accumulate_moments_var(cov=input1_cov,
                                                       mean=input1_mean,
                                                       scope='accumulate_moments_var1')

                            # ------------------------------------------------------------------------------------------
                            # Moving moments ops
                            # ------------------------------------------------------------------------------------------
                            input0_cov_, input0_mean_ = \
                                accumulate_moments_op(orig_cov=moving_cov_var0_,
                                                      orig_mean=moving_mean_var0_,
                                                      cov=input0_cov,
                                                      mean=input0_mean,
                                                      scope='accumulate_moments_op0')

                            input1_cov_, input1_mean_ = \
                                accumulate_moments_op(orig_cov=moving_cov_var1_,
                                                      orig_mean=moving_mean_var1_,
                                                      cov=input1_cov,
                                                      mean=input1_mean,
                                                      scope='accumulate_moments_op1')

                            update_moments = tf.group(update_moments_0_, update_moments_1_)

                        else:
                            input0_mean_ = tf.identity(input0_mean)
                            input0_cov_ = tf.identity(input0_cov)
                            input1_mean_ = tf.identity(input1_mean)
                            input1_cov_ = tf.identity(input1_cov)
                            
                            update_moments = tf.no_op()

                        # samv1 stop_gradiet op was here
                        input0_mean__ = tf.identity(input0_mean_)
                        input0_cov__ = tf.identity(input0_cov_)

                        # if module is used to perform kl minimization
                        if functionality == 'min':
                            print('creating a min sam module')
                            with tf.device("/cpu:0"):
                                align_loss, kl_sanity = sym_gaussian_kl(cov0=input0_cov__,
                                                                        cov1=input1_cov_,
                                                                        mean0=input0_mean__,
                                                                        mean1=input1_mean_,
                                                                        scope='min_kl_loss')

                        # if module is used to perform CORAL minimization
                        elif functionality == 'coral':
                            print('creating a coral sam module')
                            align_loss, coral_sanity = coral_loss_function(input0_cov__, input1_cov_)

                        else:
                            raise Exception, 'no such sam functionality'

            sanity_check = 1.0

            return align_loss, update_moments, sanity_check


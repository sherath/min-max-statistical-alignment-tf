from __future__ import division
import os
import numpy as np

from scipy.io import loadmat


class Sun(object):
    
    def __init__(self, datadir=None):

        """Load the Sun data 
        
        att_splits.mat  : contains the following
        
        allclasses_names : 717 x 1 vector with names
        att              : 102 x 717 matrix (102 attributes and 717 classes)
        test_seen_loc    : test indices for the 2580 x 1 vector (seen classes)
        test_unseen_loc  : test indices for the 1440 x 1 vector (unseen classes)
        train_loc        : train indices 11600 x 1 vector
        train_val_loc    : train+val indices 10320 x 1 vector
        val_loc          : val indices 1300 x 1 vector
        
        binaryAtt_splits.mat : contains the following plus the att is binary
        
        res101.mat       : contains the resnet 101 features and class labels
        
        features   : 2048 x 14340 double matrix
        labels     : 14340 x 1 labels matrix
        
        """
        
        print('Loading Sun')
        
        att_splits = loadmat(os.path.join(datadir, 'SUN/att_splits.mat'))
            
        res101 = loadmat(os.path.join(datadir, 'SUN/res101.mat'))
                        
        features = self.format_features(res101['features'])
        labels   = np.squeeze(res101['labels']-1)

        train_locs       = np.squeeze(att_splits['train_loc'] - 1)
        # val_locs         = np.squeeze(att_splits['val_loc'] - 1)
        train_val_locs   = np.squeeze(att_splits['trainval_loc'] - 1)
        test_seen_locs   = np.squeeze(att_splits['test_seen_loc'] - 1)
        test_unseen_locs = np.squeeze(att_splits['test_unseen_loc'] - 1)
        test_all_locs    = np.concatenate((test_seen_locs, test_unseen_locs) , axis=0)
        
        # indexing features and labels
        train_feats_x = features[train_locs,:,:,:]
        train_feats_y = labels[train_locs]
        
        train_val_feats_x = features[train_val_locs,:,:,:]
        train_val_feats_y = labels[train_val_locs]       
        
        test_feats_seen_x = features[test_seen_locs,:,:,:]
        test_feats_seen_y = labels[test_seen_locs]
        
        test_feats_unseen_x = features[test_unseen_locs,:,:,:]
        test_feats_unseen_y = labels[test_unseen_locs] 

        test_feats_all_x = features[test_all_locs,:,:,:]
        test_feats_all_y = labels[test_all_locs] 
       
        #----------------------------------------------------------------------
        # Data with the original class labels from the Awa1 set
        #----------------------------------------------------------------------
        
        self.att = self.format_features(att_splits['att'])   # attribute matrix N_cls x N_att matrix (eg. here a 50 x 85 matrix)
        self.train_feats       = {'x' : train_feats_x     , 'y' : train_feats_y}
        self.train_val_feats   = {'x' : train_val_feats_x , 'y' : train_val_feats_y}
        self.test_feats_seen   = {'x' : test_feats_seen_x , 'y' : test_feats_seen_y}     # only seen class test features 
        self.test_feats_unseen = {'x' : test_feats_unseen_x , 'y' : test_feats_unseen_y} # only unseen class test features
        self.test_feats_all    = {'x' : test_feats_all_x    , 'y' : test_feats_all_y}    # seen and unseen class test features
        self.unseen_classes    = np.setdiff1d(test_feats_all_y, train_val_feats_y)
        self.seen_classes      = np.setdiff1d(test_feats_all_y, self.unseen_classes)
        
        #---------------------------------------------------------------------------------
        # Data with the changed class labels from the Awa1 set removing the unseen classes(labels are re-indexed from 0 - num_of_seen_classes)
        #---------------------------------------------------------------------------------        
 
        gan_stage_original_labels_set = np.unique(train_val_feats_y)
        gan_stage_new_labels_set      = np.arange(len(gan_stage_original_labels_set))
    
        swaped_train_val_feats_y  = self.swap_labels(train_val_feats_y, gan_stage_original_labels_set, gan_stage_new_labels_set)    
        swaped_test_feats_seen_y  = self.swap_labels(test_feats_seen_y, gan_stage_original_labels_set, gan_stage_new_labels_set)  
        swapped_attributes        = self.swap_attributes(self.att, gan_stage_original_labels_set, gan_stage_new_labels_set )
    
        self.gan_stage_train_feats  = {'x':train_val_feats_x , 'y': swaped_train_val_feats_y } 
        self.gan_stage_test_feats   = {'x':test_feats_seen_x , 'y': swaped_test_feats_seen_y }
        self.gan_stage_att          = swapped_attributes # swapped attributes by only keeping the seen classes 
        
    @staticmethod
    def swap_attributes(att, original_labels_set, new_labels_set):

        """ builds a new attribute tensor with only the attributes in the seen classes. indexing is based on the new_labels_set """

        num_of_seen_classes = new_labels_set.size
        num_of_attributes   = att.shape[-1]
        
        swaped_att = 10000*np.ones((num_of_seen_classes, 1, 1, num_of_attributes), dtype=np.float32)
        
        for idx, label in enumerate(original_labels_set):
            swaped_att[new_labels_set[idx], :, :, :] = att[label, :, :, :]
        
        assert np.all(swaped_att < 1000)
        
        return swaped_att

        
    @staticmethod
    def swap_labels(y, original_labels_set, new_labels_set):    
        """ Swaps the labels in y as for based on the original_labels_set and the new_labels_set
        """
        # contains the new swaped variables
        swaped_y = -1*np.ones_like(y)
        
        for idx, label in enumerate(original_labels_set):
            swaped_y[y==label] = new_labels_set[idx]
        
        # some assertions to makesure the label switching is okay
        assert np.setdiff1d(swaped_y, new_labels_set).size == 0
        assert len(np.unique(y)) == len(np.unique(swaped_y))
        
        return swaped_y
        
    
    @staticmethod        
    def format_features(column_features):
        
        row_features    = np.transpose(column_features).astype('float32')
        expanded_feats  = np.expand_dims(row_features, axis =1)
        tf_features     = np.expand_dims(expanded_feats, axis =1)
        
        return tf_features


















class Cub(object):
    
    def __init__(self, datadir=None):

        """Load the Cub data 
        
        att_splits.mat  : contains the following
        
        allclasses_names : 200 x 1 vector with names
        att              : 312 x 200 matrix (312 attributes and 200 classes)
        test_seen_loc    : test indices for the 1764 x 1 vector (seen classes)
        test_unseen_loc  : test indices for the 2967 x 1 vector (unseen classes)
        train_loc        : train indices 5875 x 1 vector
        train_val_loc    : train+val indices 7057 x 1 vector
        val_loc          : val indices 2946 x 1 vector
        
        binaryAtt_splits.mat : contains the following plus the att is binary
        
        res101.mat       : contains the resnet 101 features and class labels
        
        features   : 2048 x 11788 double matrix
        labels     : 11788 x 1 labels matrix
        
        """
        
        print('Loading Cub')
        
        att_splits = loadmat(os.path.join(datadir, 'CUB/att_splits.mat'))
            
        res101 = loadmat(os.path.join(datadir, 'CUB/res101.mat'))
                        
        features = self.format_features(res101['features'])
        labels   = np.squeeze(res101['labels']-1)

        train_locs       = np.squeeze(att_splits['train_loc'] - 1)
        # val_locs         = np.squeeze(att_splits['val_loc'] - 1)
        train_val_locs   = np.squeeze(att_splits['trainval_loc'] - 1)
        test_seen_locs   = np.squeeze(att_splits['test_seen_loc'] - 1)
        test_unseen_locs = np.squeeze(att_splits['test_unseen_loc'] - 1)
        test_all_locs    = np.concatenate((test_seen_locs, test_unseen_locs) , axis=0)
        
        # indexing features and labels
        train_feats_x = features[train_locs,:,:,:]
        train_feats_y = labels[train_locs]
        
        train_val_feats_x = features[train_val_locs,:,:,:]
        train_val_feats_y = labels[train_val_locs]       
        
        test_feats_seen_x = features[test_seen_locs,:,:,:]
        test_feats_seen_y = labels[test_seen_locs]
        
        test_feats_unseen_x = features[test_unseen_locs,:,:,:]
        test_feats_unseen_y = labels[test_unseen_locs] 

        test_feats_all_x = features[test_all_locs,:,:,:]
        test_feats_all_y = labels[test_all_locs] 
       
        #----------------------------------------------------------------------
        # Data with the original class labels from the Awa1 set
        #----------------------------------------------------------------------
        
        self.att = self.format_features(att_splits['att'])   # attribute matrix N_cls x N_att matrix (eg. here a 50 x 85 matrix)
        self.train_feats       = {'x' : train_feats_x     , 'y' : train_feats_y}
        self.train_val_feats   = {'x' : train_val_feats_x , 'y' : train_val_feats_y}
        self.test_feats_seen   = {'x' : test_feats_seen_x , 'y' : test_feats_seen_y}     # only seen class test features 
        self.test_feats_unseen = {'x' : test_feats_unseen_x , 'y' : test_feats_unseen_y} # only unseen class test features
        self.test_feats_all    = {'x' : test_feats_all_x    , 'y' : test_feats_all_y}    # seen and unseen class test features
        self.unseen_classes    = np.setdiff1d(test_feats_all_y, train_val_feats_y)
        self.seen_classes      = np.setdiff1d(test_feats_all_y, self.unseen_classes)

        
        #---------------------------------------------------------------------------------
        # Data with the changed class labels from the Awa1 set removing the unseen classes(labels are re-indexed from 0 - num_of_seen_classes)
        #---------------------------------------------------------------------------------        
 
        gan_stage_original_labels_set = np.unique(train_val_feats_y)
        gan_stage_new_labels_set      = np.arange(len(gan_stage_original_labels_set))
    
        swaped_train_val_feats_y  = self.swap_labels(train_val_feats_y, gan_stage_original_labels_set, gan_stage_new_labels_set)    
        swaped_test_feats_seen_y  = self.swap_labels(test_feats_seen_y, gan_stage_original_labels_set, gan_stage_new_labels_set)  
        swapped_attributes        = self.swap_attributes(self.att, gan_stage_original_labels_set, gan_stage_new_labels_set )
    
        self.gan_stage_train_feats  = {'x':train_val_feats_x , 'y': swaped_train_val_feats_y } 
        self.gan_stage_test_feats   = {'x':test_feats_seen_x , 'y': swaped_test_feats_seen_y }
        self.gan_stage_att          = swapped_attributes # swapped attributes by only keeping the seen classes 
        
    @staticmethod
    def swap_attributes(att, original_labels_set, new_labels_set):

        """ builds a new attribute tensor with only the attributes in the seen classes. indexing is based on the new_labels_set """

        num_of_seen_classes = new_labels_set.size
        num_of_attributes   = att.shape[-1]
        
        swaped_att = 10000*np.ones((num_of_seen_classes, 1, 1, num_of_attributes), dtype=np.float32)
        
        for idx, label in enumerate(original_labels_set):
            swaped_att[new_labels_set[idx], :, :, :] = att[label, :, :, :]
        
        assert np.all(swaped_att < 1000)
        
        return swaped_att

        
    @staticmethod
    def swap_labels(y, original_labels_set, new_labels_set):    
        """ Swaps the labels in y as for based on the original_labels_set and the new_labels_set
        """
        # contains the new swaped variables
        swaped_y = -1*np.ones_like(y)
        
        for idx, label in enumerate(original_labels_set):
            swaped_y[y==label] = new_labels_set[idx]
        
        # some assertions to makesure the label switching is okay
        assert np.setdiff1d(swaped_y, new_labels_set).size == 0
        assert len(np.unique(y)) == len(np.unique(swaped_y))
        
        return swaped_y
        
    
    @staticmethod        
    def format_features(column_features):
        
        row_features    = np.transpose(column_features).astype('float32')
        expanded_feats  = np.expand_dims(row_features, axis =1)
        tf_features     = np.expand_dims(expanded_feats, axis =1)
        
        return tf_features














class Awa1(object):
    
    def __init__(self, datadir=None):

        """Load the Awa1 data 
        
        att_splits.mat  : contains the following
        
        allclasses_names : 50 x 1 vector with names
        att              : 85 x 50 matrix (85 attributes and 50 classes)
        test_seen_loc    : test indices for the 4958 x 1 vector (seen classes)
        test_unseen_loc  : test indices for the 5685 x 1 vector (unseen classes)
        train_loc        : train indices 16864 x 1 vector
        train_val_loc    : train+val indices 19832 x 1 vector
        val_loc          : val indices 7926 x 1 vector
        
        binaryAtt_splits.mat : contains the following plus the att is binary
        
        res101.mat       : contains the resnet 101 features and class labels
        
        features   : 2048 x 30475 double matrix
        labels     : 30475 x 1 labels matrix
        
        """
        
        print('Loading Awa1')
        
        att_splits = loadmat(os.path.join(datadir, 'AWA1/att_splits.mat'))
            
        res101 = loadmat(os.path.join(datadir, 'AWA1/res101.mat'))
                        
        features = self.format_features(res101['features'])
        labels   = np.squeeze(res101['labels']-1)

        train_locs       = np.squeeze(att_splits['train_loc'] - 1)
        # val_locs         = np.squeeze(att_splits['val_loc'] - 1)
        train_val_locs   = np.squeeze(att_splits['trainval_loc'] - 1)
        test_seen_locs   = np.squeeze(att_splits['test_seen_loc'] - 1)
        test_unseen_locs = np.squeeze(att_splits['test_unseen_loc'] - 1)
        test_all_locs    = np.concatenate((test_seen_locs, test_unseen_locs) , axis=0)
        
        # indexing features and labels
        train_feats_x = features[train_locs,:,:,:]
        train_feats_y = labels[train_locs]
        
        train_val_feats_x = features[train_val_locs,:,:,:]
        train_val_feats_y = labels[train_val_locs]       
        
        test_feats_seen_x = features[test_seen_locs,:,:,:]
        test_feats_seen_y = labels[test_seen_locs]
        
        test_feats_unseen_x = features[test_unseen_locs,:,:,:]
        test_feats_unseen_y = labels[test_unseen_locs] 

        test_feats_all_x = features[test_all_locs,:,:,:]
        test_feats_all_y = labels[test_all_locs] 
       
        #----------------------------------------------------------------------
        # Data with the original class labels from the Awa1 set
        #----------------------------------------------------------------------
        
        self.att = self.format_features(att_splits['att'])   # attribute matrix N_cls x N_att matrix (eg. here a 50 x 85 matrix)
        self.train_feats       = {'x' : train_feats_x     , 'y' : train_feats_y}
        self.train_val_feats   = {'x' : train_val_feats_x , 'y' : train_val_feats_y}
        self.test_feats_seen   = {'x' : test_feats_seen_x , 'y' : test_feats_seen_y}     # only seen class test features 
        self.test_feats_unseen = {'x' : test_feats_unseen_x , 'y' : test_feats_unseen_y} # only unseen class test features
        self.test_feats_all    = {'x' : test_feats_all_x    , 'y' : test_feats_all_y}    # seen and unseen class test features
        self.unseen_classes    = np.setdiff1d(test_feats_all_y, train_val_feats_y)
        self.seen_classes      = np.setdiff1d(test_feats_all_y, self.unseen_classes)
        
        #---------------------------------------------------------------------------------
        # Data with the changed class labels from the Awa1 set removing the unseen classes(labels are re-indexed from 0 - num_of_seen_classes)
        #---------------------------------------------------------------------------------        
 
        gan_stage_original_labels_set = np.unique(train_val_feats_y)
        gan_stage_new_labels_set      = np.arange(len(gan_stage_original_labels_set))
    
        swaped_train_val_feats_y  = self.swap_labels(train_val_feats_y, gan_stage_original_labels_set, gan_stage_new_labels_set)    
        swaped_test_feats_seen_y  = self.swap_labels(test_feats_seen_y, gan_stage_original_labels_set, gan_stage_new_labels_set)  
        swapped_attributes        = self.swap_attributes(self.att, gan_stage_original_labels_set, gan_stage_new_labels_set )
    
        self.gan_stage_train_feats  = {'x':train_val_feats_x , 'y': swaped_train_val_feats_y } 
        self.gan_stage_test_feats   = {'x':test_feats_seen_x , 'y': swaped_test_feats_seen_y }
        self.gan_stage_att          = swapped_attributes # swapped attributes by only keeping the seen classes 
        
    @staticmethod
    def swap_attributes(att, original_labels_set, new_labels_set):

        """ builds a new attribute tensor with only the attributes in the seen classes. indexing is based on the new_labels_set """

        num_of_seen_classes = new_labels_set.size
        num_of_attributes   = att.shape[-1]
        
        swaped_att = 10000*np.ones((num_of_seen_classes, 1, 1, num_of_attributes), dtype=np.float32)
        
        for idx, label in enumerate(original_labels_set):
            swaped_att[new_labels_set[idx], :, :, :] = att[label, :, :, :]
        
        assert np.all(swaped_att < 1000)
        
        return swaped_att

        
    @staticmethod
    def swap_labels(y, original_labels_set, new_labels_set):    
        """ Swaps the labels in y as for based on the original_labels_set and the new_labels_set
        """
        # contains the new swaped variables
        swaped_y = -1*np.ones_like(y)
        
        for idx, label in enumerate(original_labels_set):
            swaped_y[y==label] = new_labels_set[idx]
        
        # some assertions to makesure the label switching is okay
        assert np.setdiff1d(swaped_y, new_labels_set).size == 0
        assert len(np.unique(y)) == len(np.unique(swaped_y))
        
        return swaped_y
        
    
    @staticmethod        
    def format_features(column_features):
        
        row_features    = np.transpose(column_features).astype('float32')
        expanded_feats  = np.expand_dims(row_features, axis =1)
        tf_features     = np.expand_dims(expanded_feats, axis =1)
        
        return tf_features







class Awa2(object):
    
    def __init__(self, datadir=None, use_binary_att=False):

        """Load the Awa2 data 
        
        att_splits.mat  : contains the following
        
        allclasses_names : 50 x 1 vector with names
        att              : 85 x 50 matrix (85 attributes and 50 classes)
        test_seen_loc    : test indices for the 5882 x 1 vector (seen classes)
        test_unseen_loc  : test indices for the 7913 x 1 vector (unseen classes)
        train_loc        : train indices 20218 x 1 vector
        train_val_loc    : train+val indices 23527 x 1 vector
        val_loc          : val indices 9191 x 1 vector
        
        binaryAtt_splits.mat : contains the following plus the att is binary
        
        res101.mat       : contains the resnet 101 features and class labels
        
        features   : 2048 x 37322 double matrix
        labels     : 37322 x 1 labels matrix
        
        """
        
        print('Loading Awa2')
        
        if use_binary_att:
            att_splits = loadmat(os.path.join(datadir, 'AWA2/binaryAtt_splits.mat'))
        else:
            att_splits = loadmat(os.path.join(datadir, 'AWA2/att_splits.mat'))
            
        res101 = loadmat(os.path.join(datadir, 'AWA2/res101.mat'))
                        
        features = self.format_features(res101['features'])
        labels   = np.squeeze(res101['labels']-1)

        train_locs       = np.squeeze(att_splits['train_loc'] - 1)
        # val_locs         = np.squeeze(att_splits['val_loc'] - 1)
        train_val_locs   = np.squeeze(att_splits['trainval_loc'] - 1)
        test_seen_locs   = np.squeeze(att_splits['test_seen_loc'] - 1)
        test_unseen_locs = np.squeeze(att_splits['test_unseen_loc'] - 1)
        test_all_locs    = np.concatenate((test_seen_locs, test_unseen_locs) , axis=0)
        
        # indexing features and labels
        train_feats_x = features[train_locs,:,:,:]
        train_feats_y = labels[train_locs]
        
        train_val_feats_x = features[train_val_locs,:,:,:]
        train_val_feats_y = labels[train_val_locs]       
        
        test_feats_seen_x = features[test_seen_locs,:,:,:]
        test_feats_seen_y = labels[test_seen_locs]
        
        test_feats_unseen_x = features[test_unseen_locs,:,:,:]
        test_feats_unseen_y = labels[test_unseen_locs] 

        test_feats_all_x = features[test_all_locs,:,:,:]
        test_feats_all_y = labels[test_all_locs] 
       
        #----------------------------------------------------------------------
        # Data with the original class labels from the Awa1 set
        #----------------------------------------------------------------------
        
        self.att = self.format_features(att_splits['att'])   # attribute matrix N_cls x N_att matrix (eg. here a 50 x 85 matrix)
        self.train_feats       = {'x' : train_feats_x     , 'y' : train_feats_y}
        self.train_val_feats   = {'x' : train_val_feats_x , 'y' : train_val_feats_y}
        self.test_feats_seen   = {'x' : test_feats_seen_x , 'y' : test_feats_seen_y}     # only seen class test features 
        self.test_feats_unseen = {'x' : test_feats_unseen_x , 'y' : test_feats_unseen_y} # only unseen class test features
        self.test_feats_all    = {'x' : test_feats_all_x    , 'y' : test_feats_all_y}    # seen and unseen class test features
        self.unseen_classes    = np.setdiff1d(test_feats_all_y, train_val_feats_y)
        self.seen_classes      = np.setdiff1d(test_feats_all_y, self.unseen_classes)
        
        #---------------------------------------------------------------------------------
        # Data with the changed class labels from the Awa1 set removing the unseen classes(labels are re-indexed from 0 - num_of_seen_classes)
        #---------------------------------------------------------------------------------        
 
        gan_stage_original_labels_set = np.unique(train_val_feats_y)
        gan_stage_new_labels_set      = np.arange(len(gan_stage_original_labels_set))
        
    
        swaped_train_val_feats_y  = self.swap_labels(train_val_feats_y, gan_stage_original_labels_set, gan_stage_new_labels_set)    
        swaped_test_feats_seen_y  = self.swap_labels(test_feats_seen_y, gan_stage_original_labels_set, gan_stage_new_labels_set)  
        swapped_attributes        = self.swap_attributes(self.att, gan_stage_original_labels_set, gan_stage_new_labels_set )
    
        self.gan_stage_train_feats  = {'x':train_val_feats_x , 'y': swaped_train_val_feats_y } 
        self.gan_stage_test_feats   = {'x':test_feats_seen_x , 'y': swaped_test_feats_seen_y }
        self.gan_stage_att          = swapped_attributes # swapped attributes by only keeping the seen classes 
    
    @staticmethod
    def swap_attributes(att, original_labels_set, new_labels_set):

        """ builds a new attribute tensor with only the attributes in the seen classes. indexing is based on the new_labels_set """

        num_of_seen_classes = new_labels_set.size
        num_of_attributes   = att.shape[-1]
        
        swaped_att = 10000*np.ones((num_of_seen_classes, 1, 1, num_of_attributes), dtype=np.float32)
        
        for idx, label in enumerate(original_labels_set):
            swaped_att[new_labels_set[idx], :, :, :] = att[label, :, :, :]
        
        assert np.all(swaped_att < 1000)
        
        return swaped_att
        
    @staticmethod
    def swap_labels(y, original_labels_set, new_labels_set):    
        """ Swaps the labels in y as for based on the original_labels_set and the new_labels_set
        """
        # contains the new swaped variables
        swaped_y = -1*np.ones_like(y)
        
        for idx, label in enumerate(original_labels_set):
            swaped_y[y==label] = new_labels_set[idx]
        
        # some assertions to makesure the label switching is okay
        assert np.setdiff1d(swaped_y, new_labels_set).size == 0
        assert len(np.unique(y)) == len(np.unique(swaped_y))
        
        return swaped_y        
        
    @staticmethod        
    def format_features(column_features):
        
        row_features    = np.transpose(column_features).astype('float32')
        expanded_feats  = np.expand_dims(row_features, axis =1)
        tf_features     = np.expand_dims(expanded_feats, axis =1)
        
        return tf_features
        

def get_data(dataset_id, datadir):
    """Returns Domain object based on domain_id
    """
    
    if dataset_id == 'Awa2':
        return Awa2(datadir=datadir)

    elif dataset_id == 'Awa1':
        return Awa1(datadir=datadir)
        
    elif dataset_id == 'Cub':
        return Cub(datadir=datadir)

    elif dataset_id == 'Sun':
        return Sun(datadir=datadir)

    else:
        raise Exception('dataset {:s} not recognized'.format(dataset_id))

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
'''
'''

from __future__ import division

import numpy as np
import random

import nn_models
import Train_Eval_ZSL
import Train_Eval_Generator
# from samv1_1.sam import sam
from samv1_1.sam import sam

import tensorflow as tf
from tensorflow.contrib.framework import arg_scope
slim = tf.contrib.slim


class Siamese(Train_Eval_ZSL.Train_Eval_ZSL, Train_Eval_Generator.Train_Eval_Generator):

    def __init__(self,
                 sess,
                 data_object,
                 SETUP_PARAMS):

        # ---------------------------------------------------------------------
        # Placeholders init
        # ---------------------------------------------------------------------
        self.input_feat_x = None
        self.input_att_x = None
        self.image_labels = None
        self.classifier_feat_x = None

        # ---------------------------------------------------------------------
        # alpha weights init
        # ---------------------------------------------------------------------
        self.alpha_sam = None
        self.alpha_weight_gen_cls = None

        # ---------------------------------------------------------------------
        # gamma switches init
        # ---------------------------------------------------------------------
        self.gamma_sam = None
        self.gamma_gen_cls = None
        self.gamma_real_cls = None

        # ---------------------------------------------------------------------
        # Network outputs
        # ---------------------------------------------------------------------
        self.mid_feat_x = None
        self.real_feats = None
        self.mid_feat_x_val = None

        self.gen_mid_feat_x = None
        self.gen_feats = None
        self.gen_mid_feat_x_val = None

        self.seen_logits_x = None
        self.gen_seen_logits_x = None
        self.seen_logits_x_val = None
        self.gen_seen_logits_x_val = None

        self.full_logits_x = None
        self.full_logits_x_val = None

        # ---------------------------------------------------------------------
        # loss ops
        # ---------------------------------------------------------------------
        # real data classification loss
        self.real_seen_train_loss = None
        self.real_seen_val_loss = None
        self.gen_seen_train_loss = None
        self.scaled_gen_seen_train_loss = None
        self.gen_seen_val_loss = None

        self.full_train_loss = None
        self.full_val_loss = None

        # ---------------------------------------------------------------------
        # SAM ops
        # ---------------------------------------------------------------------
        self.stat_align_loss = None
        self.scaled_stat_align_loss = None
        self.update_moments = None
        self.sam_sanity_check = None

        # ---------------------------------------------------------------------
        # Training loss ops
        # ---------------------------------------------------------------------
        self.feature_extractor_train_loss = None
        self.generator_train_loss = None

        # ---------------------------------------------------------------------
        # Evaluation metrics
        # ---------------------------------------------------------------------
        self.real_seen_train_acc = None
        self.gen_seen_train_acc = None
        self.real_seen_val_acc = None
        self.gen_seen_val_acc = None
        self.full_train_acc = None
        self.full_val_acc = None

        # ---------------------------------------------------------------------
        # Model variable collections
        # ---------------------------------------------------------------------
        self.feature_extractor_variables = None
        self.generator_function_variables = None
        self.sam_variables = None
        self.seen_classifier_variables = None
        self.full_classifier_variables = None

        # fix the random seed for the experiment-------------------------------
        tf.set_random_seed(0)
        np.random.seed(0)
        random.seed(0)
        # ---------------------------------------------------------------------

        # Methods passed in :
        # network_function  : the network structure build with tf.slim package

        # Training and session
        self.sess = sess
        self.train_params = SETUP_PARAMS['train_params']

        # Network model functions
        self.network_arg_scope = getattr(nn_models, 'network_arg_scope')

        # Shared classifier for real and generated features includes only seen classes
        self.classifier_function = getattr(nn_models, self.train_params['classifier_model'])

        # Feature generation from attributes
        self.generator_function = getattr(nn_models, self.train_params['generator_model'])

        # Seen image feature extractor
        self.feature_extractor_function = getattr(nn_models, self.train_params['feature_extractor'])

        # Confusion function for min-max sam module
        self.confusion_function = getattr(nn_models, self.train_params['confusion_model'])

        # Dataset objects
        self.data_object = data_object

        self.model_train_losses()

    def model_train_losses(self):

        input_feat_dims = [1, 1, 2048]
        input_att_dims = [1, 1, self.train_params['num_attributes']]

        classifier_feat_dim = [1, 1, self.train_params['classifier_feat_dim']]  # should be equal to the feature extractor's dimensions and the generator's output dimension

        # ----------------------------------------------------------------------
        # Create a placeholder for the network inputs
        # ----------------------------------------------------------------------
        with tf.name_scope('input_placeholders_generator_training'):

            self.input_feat_x = tf.placeholder(tf.float32, [None] + input_feat_dims, name='input_feat_x')
            self.input_att_x = tf.placeholder(tf.float32, [None] + input_att_dims, name='input_att_x')
            self.image_labels = tf.placeholder(tf.int32, [None], name='image_labels')

        with tf.name_scope('input_placeholders_full_classifier_training'):

            # inputs to the final classifier (real input features, generated seen class features,
            # generated unseen class features at train time)
            self.classifier_feat_x = tf.placeholder(tf.float32, [None] + classifier_feat_dim, name='classifier_feat_x')

        # ----------------------------------------------------------------------
        # alpha Weights for the losses
        # ----------------------------------------------------------------------
        with tf.name_scope('alpha_loss_weights'):

            self.alpha_sam = tf.constant(self.train_params['alpha_weight_sam'])  # Weight on the sam loss
            self.alpha_weight_gen_cls = tf.constant(self.train_params['alpha_weight_gen_cls'])  # Weight on the generated data classification

        # ----------------------------------------------------------------------
        # gamma switches for alternating between losses
        # ----------------------------------------------------------------------
        with tf.name_scope('gamma_loss_switches'):

            self.gamma_sam = tf.placeholder(tf.float32, shape=(), name='gamma_sam')  # Weight on the overall adverserial statistic loss for the basenetwork training (for the first 100 epochs it is 0, then switched up)
            self.gamma_gen_cls = tf.placeholder(tf.float32, shape=(), name='gamma_gen_cls')  # Weight on the overall direct statistic loss for the basenetwork training (for the first 100 epochs it is 0, then switched up)
            self.gamma_real_cls = tf.placeholder(tf.float32, shape=(), name='gamma_real_cls')  # Weight on the target discriminator

        # ----------------------------------------------------------------------
        # Feature Extractor from images 
        # ----------------------------------------------------------------------
        with tf.variable_scope('feature_extractor_function') as sc_fef:
            with slim.arg_scope(self.network_arg_scope()):

                self.mid_feat_x = self.feature_extractor_function(inputs=self.input_feat_x,
                                                                  is_training=True,
                                                                  reuse=False,
                                                                  output_dim=classifier_feat_dim[-1])
                if self.train_params['use_compact_feats']:
                    self.real_feats = tf.nn.tanh(self.mid_feat_x)
                else:
                    self.real_feats = tf.identity(self.mid_feat_x)

                sc_fef.reuse_variables()  # reuse the variables for the validation phase

                self.mid_feat_x_val = self.feature_extractor_function(inputs=self.input_feat_x,
                                                                      is_training=False,
                                                                      reuse=False,
                                                                      output_dim=classifier_feat_dim[-1])

        # ---------------------------------------------------------------------
        # Feature generation from attributes
        # ---------------------------------------------------------------------
        with tf.variable_scope('generator_function') as sc_gen:
            with slim.arg_scope(self.network_arg_scope()):

                self.gen_mid_feat_x = self.generator_function(inputs=self.input_att_x,
                                                              is_training=True,
                                                              reuse=False,
                                                              output_dim=classifier_feat_dim[-1])
                if self.train_params['use_compact_feats']:
                    self.gen_feats = tf.nn.tanh(self.gen_mid_feat_x)
                else:
                    self.gen_feats = tf.identity(self.gen_mid_feat_x)
                
                sc_gen.reuse_variables()  # reuse the variables for the validation phase

                self.gen_mid_feat_x_val = self.generator_function(inputs=self.input_att_x,
                                                                  is_training=False,
                                                                  reuse=False,
                                                                  output_dim=classifier_feat_dim[-1])

        # ----------------------------------------------------------------------
        # Seen class features classifier (while training the generator)
        # ----------------------------------------------------------------------
        with tf.variable_scope('seen_classifier_function') as sc_scf:
            with slim.arg_scope(self.network_arg_scope()):
                
                self.seen_logits_x = self.classifier_function(inputs=self.mid_feat_x,
                                                              is_training=True,
                                                              reuse=False,
                                                              num_classes=self.train_params['num_seen_classes'])
               
                sc_scf.reuse_variables()  # reuse the variables for training on the generated data as well
                
                self.gen_seen_logits_x = self.classifier_function(inputs=self.gen_mid_feat_x,
                                                                  is_training=True,
                                                                  reuse=False,
                                                                  num_classes=self.train_params['num_seen_classes'])

                self.seen_logits_x_val = self.classifier_function(inputs=self.mid_feat_x_val,
                                                                  is_training=False,
                                                                  reuse=False,
                                                                  num_classes=self.train_params['num_seen_classes'])

                self.gen_seen_logits_x_val = self.classifier_function(inputs=self.gen_mid_feat_x_val,
                                                                      is_training=False,
                                                                      reuse=False,
                                                                      num_classes=self.train_params['num_seen_classes'])

        # ---------------------------------------------------------------------
        # SAM : Statistical Alignment Module
        # ---------------------------------------------------------------------
        with tf.variable_scope('statistical_alignment_module'):
            with arg_scope([self.confusion_function],
                           num_layers=self.train_params['conf_num_layers'],
                           mid_activation=self.train_params['conf_mid_activation'],
                           mid_layer_dim=self.train_params['conf_mid_layer_dim'],
                           use_bnorm=self.train_params['conf_use_bnorm'],
                           use_compact_feats=self.train_params['use_compact_feats']):

                self.stat_align_loss, self.update_moments, self.sam_sanity_check = \
                    sam(input0=self.real_feats,
                        input1=self.gen_feats,
                        squeeze_inputs=False,
                        stop_grad0=True,
                        unit_normal0=False,
                        conf_network=self.confusion_function,
                        conf_net_out_dim=self.train_params['confused_feat_dim'],  # classifier_feat_dim[-1],
                        share_conf_network=True,
                        use_accumulate_moments=self.train_params['sam_use_accumulate_moments'],
                        momentum=self.train_params['sam_momentum'],
                        use_diag=self.train_params['sam_use_diag'],
                        batch_size=self.train_params['train_batch_size'],
                        cov_reg=1e-5,
                        functionality=self.train_params['sam_functionality'],
                        scope='stat_align_module')

            self.scaled_stat_align_loss = tf.scalar_mul(self.alpha_sam, self.stat_align_loss)

        # ----------------------------------------------------------------------
        # Full class classification function (for zsl on generated + real data)
        # ----------------------------------------------------------------------
        with tf.variable_scope('full_classifier_function') as sc_full:
            with slim.arg_scope(self.network_arg_scope()):
                
                total_num_classes = self.train_params['num_seen_classes'] + self.train_params['num_unseen_classes']
                              
                self.full_logits_x = self.classifier_function(inputs=self.classifier_feat_x,
                                                              is_training=True,
                                                              reuse=False,
                                                              num_classes=total_num_classes)
                sc_full.reuse_variables()  # reuse the variables for confusing the generated features from attributes
                
                self.full_logits_x_val = self.classifier_function(inputs=self.classifier_feat_x,
                                                                  is_training=False,
                                                                  reuse=False,
                                                                  num_classes=total_num_classes)

        # ----------------------------------------------------------------------
        # Some loss function definitions
        # ----------------------------------------------------------------------
        def softmax_cross_entropy_with_logits(x, y, num_classes):
            one_hot_labels = slim.one_hot_encoding(y, num_classes)
            return tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=x, labels=one_hot_labels))

        # ----------------------------------------------------------------------
        # Classification losses
        # ----------------------------------------------------------------------
        with tf.name_scope('seen_classification_losses'):

            # real data classification loss
            self.real_seen_train_loss = softmax_cross_entropy_with_logits(self.seen_logits_x, self.image_labels,
                                                                          num_classes=self.train_params['num_seen_classes'])
            self.real_seen_val_loss = softmax_cross_entropy_with_logits(self.seen_logits_x_val, self.image_labels,
                                                                        num_classes=self.train_params['num_seen_classes'])

            # generated data classification loss
            self.gen_seen_train_loss = softmax_cross_entropy_with_logits(self.gen_seen_logits_x, self.image_labels,
                                                                         num_classes=self.train_params['num_seen_classes'])
            self.scaled_gen_seen_train_loss = tf.scalar_mul(self.alpha_weight_gen_cls, self.gen_seen_train_loss)    
            self.gen_seen_val_loss = softmax_cross_entropy_with_logits(self.gen_seen_logits_x_val, self.image_labels,
                                                                       num_classes=self.train_params['num_seen_classes'])

        with tf.name_scope('full_classification_losses'):

            total_num_classes = self.train_params['num_seen_classes'] + self.train_params['num_unseen_classes']

            # real data classification loss
            self.full_train_loss = softmax_cross_entropy_with_logits(self.full_logits_x, self.image_labels,
                                                                     num_classes=total_num_classes)
            self.full_val_loss = softmax_cross_entropy_with_logits(self.full_logits_x_val, self.image_labels,
                                                                   num_classes=total_num_classes)

        with tf.name_scope('training_losses'):

            self.feature_extractor_train_loss = tf.identity(self.real_seen_train_loss)  # Real data classification loss (seen classes)

            # classification, direct/confused statistics alignment and target's cross entropy regularizer
            self.generator_train_loss = tf.add_n([tf.scalar_mul(self.gamma_gen_cls, self.scaled_gen_seen_train_loss),  # Generated data classification loss (seen classes)
                                                  tf.scalar_mul(self.gamma_sam, self.scaled_stat_align_loss)])  # SAM alignment

        # ----------------------------------------------------------------------
        # Evaluation Metrics
        # ----------------------------------------------------------------------
        with tf.name_scope('classification_accs_seen_classes'):

            # training accuracies
            self.real_seen_train_acc = tf.reduce_mean(tf.cast(tf.equal(tf.to_int32(tf.argmax(self.seen_logits_x, 1)), self.image_labels), tf.float32))
            self.gen_seen_train_acc = tf.reduce_mean(tf.cast(tf.equal(tf.to_int32(tf.argmax(self.gen_seen_logits_x, 1)), self.image_labels), tf.float32))

            # validation accuracies
            self.real_seen_val_acc = tf.reduce_mean(tf.cast(tf.equal(tf.to_int32(tf.argmax(self.seen_logits_x_val, 1)), self.image_labels), tf.float32))
            self.gen_seen_val_acc = tf.reduce_mean(tf.cast(tf.equal(tf.to_int32(tf.argmax(self.gen_seen_logits_x_val, 1)), self.image_labels), tf.float32))

        with tf.name_scope('classification_accs_full_classes'):

            self.full_train_acc = tf.reduce_mean(tf.cast(tf.equal(tf.to_int32(tf.argmax(self.full_logits_x, 1)), self.image_labels), tf.float32))
            self.full_val_acc = tf.reduce_mean(tf.cast(tf.equal(tf.to_int32(tf.argmax(self.full_logits_x_val, 1)), self.image_labels), tf.float32))

        # ----------------------------------------------------------------------
        # Model variable segmentation : Segements the model variables
        # ----------------------------------------------------------------------

        full_var_list = [v for v in tf.global_variables()]  # All variables in the model
        vars_excluded_from_train = self.train_params['excluded_training_variables']  # moving_variance and moving_average are not updated by gradient descent 

        # ----------------------------------------------------------------------
        # Variables to train on the model
        # ----------------------------------------------------------------------
        self.feature_extractor_variables = self.subset_load_variables(full_var_list, ['feature_extractor_function'], vars_excluded_from_train)
        self.generator_function_variables = self.subset_load_variables(full_var_list, ['generator_function'], vars_excluded_from_train)
        self.sam_variables = self.subset_load_variables(full_var_list, ['statistical_alignment_module'], vars_excluded_from_train)

        self.seen_classifier_variables = self.subset_load_variables(full_var_list, ['seen_classifier_function'], vars_excluded_from_train)
        self.full_classifier_variables = self.subset_load_variables(full_var_list, ['full_classifier_function'], vars_excluded_from_train)

        print('========= Feature Extractor Trained Variables ================')
        for tempvar in self.feature_extractor_variables:
            print(tempvar.op.name)
        print('==============================================================')

        print('  ')

        print('=============== Generator Trained Variables ==================')
        for tempvar in self.generator_function_variables:
            print(tempvar.op.name)
        print('==============================================================')

        print('  ')

        print('==================== SAM Trained Variables ===================')
        for tempvar in self.sam_variables:
            print(tempvar.op.name)
        print('==============================================================')

        print('  ')

        print('============== Seen Classifier Trained Variables =============')
        for tempvar in self.seen_classifier_variables:
            print(tempvar.op.name)
        print('==============================================================')

        print('  ')

        print('=============== Full Classifier Trained Variables ============')
        for tempvar in self.full_classifier_variables:
            print(tempvar.op.name)
        print('==============================================================')

        print('  ')

        # Initialize the Saver operators for each stream
        with tf.variable_scope('All_Savers_Restorers'):
            self.full_model_saver = tf.train.Saver(name='Full_Model')

    # --------------------------------------------------------------------------
    # Initializing methods for the model
    # --------------------------------------------------------------------------

    def subset_load_variables(self, full_var_list, include_keywords, exempt_keywords):

        included_variables = self.include_subset_load_variables(full_var_list,include_keywords)
        return self.exclude_subset_load_variables(included_variables,exempt_keywords)

    # Include a subset of variables to be loaded 
    def include_subset_load_variables(self, full_var_list, include_keywords):

        include_vars = list()
        for v in full_var_list:
            if any(include_keyword in v.op.name for include_keyword in include_keywords):
                include_vars.append(v)

        return include_vars

    # Exempt a subset of variables to be loaded
    def exclude_subset_load_variables(self, full_var_list, exempt_keywords):
           
           exempt_vars = list()
           for v in full_var_list:
               if any(exempt_keyword in v.op.name for exempt_keyword in exempt_keywords):
                   exempt_vars.append(v)
           
           return list(set(full_var_list) -set(exempt_vars))

    def save_checkpoint(self, checkpoint_prefix, checkpoint_index):

           checkpoint_save_path = self.train_params['checkpoint_dir'] + '/' + checkpoint_prefix + '-' + str(checkpoint_index) + '.ckpt'
           self.full_model_saver.save(self.sess, checkpoint_save_path)
           print('checkpoint saved : ' + checkpoint_save_path)






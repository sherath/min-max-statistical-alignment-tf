#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 16:56:14 2018

@author: her171
"""
from __future__ import division
import numpy as np
import scipy
import time
import random

import tensorflow as tf
slim = tf.contrib.slim


class Train_Eval_ZSL:

    # -------------------------------------------------------------------------
    # ZSL - Training Opts defining / ZSL - Training and Evaluation Method
    # -------------------------------------------------------------------------

    def train_eval_zsl(self):

        # fix the random seed for the experiment-------------------------------
        tf.set_random_seed(0)
        np.random.seed(0)
        random.seed(0)
        # ---------------------------------------------------------------------

        print(' ')
        print('--------------------------------------------------------------')
        print('started ZSL training full classifier')
        print('--------------------------------------------------------------')

        with tf.variable_scope('zsl_optimizers_and_train_steps'):

            optimizer_full_class = tf.train.RMSPropOptimizer(self.train_params['learning_rate_zsl'], name='optimizer_full_class')

            # -----------------------------------------------------------------
            # Full Class Classifier Train Ops
            # -----------------------------------------------------------------
            full_cls_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope=self.train_params['full_classifier_model_update_op_scope'])

            print(' ')
            print('============== seen classifier update ops ================')
            for uop in full_cls_update_ops:
                print(uop.op.name)
            print('==========================================================')

            full_cls_global_step = tf.get_variable('full_cls_global_step', [], initializer=tf.constant_initializer(0.0), trainable=False)
            full_cls_train_step = slim.learning.create_train_op(self.full_train_loss,
                                                                optimizer_full_class,
                                                                variables_to_train=self.full_classifier_variables,
                                                                global_step=full_cls_global_step,
                                                                update_ops=full_cls_update_ops)

        # ----------------------------------------------------------------------
        # Initialize the model (new variables added to the model) 
        # ----------------------------------------------------------------------
        uninitialized_vars = []
        for var in tf.global_variables():
            try:
                self.sess.run(var)
            except tf.errors.FailedPreconditionError:
                uninitialized_vars.append(var)

        print('================== unintialized variables ====================')
        for uinit_var in uninitialized_vars:
            print(uinit_var.op.name)
        print('==============================================================')

        init_new_vars_op = tf.variables_initializer(uninitialized_vars)
        self.sess.run(init_new_vars_op)

        # ----------------------------------------------------------------------
        # Load features and attribute data
        # ----------------------------------------------------------------------

        # Original attributes and class labels from the dataset
        att_x = self.data_object.att
        train_x = self.data_object.train_val_feats['x']  # Note : the dataset has validation instances. However, in this code I used val for the testing phase
        train_labels = self.data_object.train_val_feats['y']  # Hence, note the difference.
        unseen_classes = self.data_object.unseen_classes 
        seen_classes = self.data_object.seen_classes 

        assert seen_classes.size == self.train_params['num_seen_classes'], 'the number of seen classes are different'
        assert unseen_classes.size == self.train_params['num_unseen_classes'], 'the number of unseen classes are different'

        test_x_seen = self.data_object.test_feats_seen['x']
        test_labels_seen = self.data_object.test_feats_seen['y']

        test_x_unseen = self.data_object.test_feats_unseen['x']
        test_labels_unseen = self.data_object.test_feats_unseen['y']

        # the following details are used for per class accuracy evalutation 
        test_classes_seen = list(np.unique(test_labels_seen))  # Get the class labels of the seen test samples
        test_classes_unseen = list(np.unique(test_labels_unseen))  # Get the class labels of the unseen test samples
        test_classes = test_classes_seen + test_classes_unseen

        assert len(test_classes_seen) == self.train_params['num_seen_classes']
        assert len(test_classes_unseen) == self.train_params['num_unseen_classes']
        assert len(test_classes) == self.train_params['num_seen_classes'] + self.train_params['num_unseen_classes']

        # ---------------------------------------------------------------------
        # Decide the training duration
        # ---------------------------------------------------------------------
        number_of_epochs = self.train_params['number_of_zsl_epochs']
        train_batch_size = self.train_params['train_batch_size']
        val_batch_size = self.train_params['val_batch_size']
        number_of_train_samples_per_class = np.ceil(train_batch_size/self.train_params['num_seen_classes']).astype(np.int32)  # generate this number of samples per unseen class for each batch
        if number_of_train_samples_per_class == 0:
            raise Exception, 'no samples to train'
        number_of_train_samples_per_class_ = np.ceil(self.train_params['sampling_rate']*number_of_train_samples_per_class).astype(np.int32)


        # instance indexes used for iterating the samples
        train_indices_array = np.arange(len(train_labels)) 
        seen_val_indices_array = np.arange(len(test_labels_seen)) 
        unseen_val_indices_array = np.arange(len(test_labels_unseen)) 

        # ---------------------------------------------------------------------
        # Initialize the summary writer
        # ---------------------------------------------------------------------
        self.writer_zsl_train = tf.summary.FileWriter(self.train_params['summaries_dir']+'/zsl_train', self.sess.graph)
        self.writer_zsl_val = tf.summary.FileWriter(self.train_params['summaries_dir']+'/zsl_val', self.sess.graph)

        # -------------------------------------------------------------------------------
        # Get the input attribute matrix to the Generator (Only need to create this once)
        # -------------------------------------------------------------------------------
        repeated_classes_array_ = np.tile(unseen_classes, number_of_train_samples_per_class_)
        generator_input_attrib_array_ = att_x[repeated_classes_array_, :, :, :]
        generator_input_labels_list_ = np.ndarray.tolist(repeated_classes_array_)

        # Iterate through the epochs
        for ep in range(number_of_epochs):

            # Batching function
            def batching(samples, batch_size):
                return (samples[pos:pos+batch_size] for pos in xrange(0, len(samples), batch_size))

            np.random.shuffle(train_indices_array)
            np.random.shuffle(seen_val_indices_array)
            np.random.shuffle(unseen_val_indices_array)

            shuffled_train_indices = list(train_indices_array)
            shuffled_seen_val_indices = list(seen_val_indices_array)
            shuffled_unseen_val_indices = list(unseen_val_indices_array)

            # -----------------------------------------------------------------
            # Validation function - Seen val instances (Per class evaluation)
            # -----------------------------------------------------------------
            print('------------------------------------------------------------')
            print('ZSL : MCA Validation on epoch - Seen Class Instances' + str(ep))
            print('------------------------------------------------------------')
            epoch_harmonic_mca_test = 0.0
            epoch_mean_mca_test = 0.0

            epoch_mca_test_seen = 0.0
            seen_class_mca_vec = -1*np.ones(len(test_classes_seen))

            for class_number, class_label in enumerate(test_classes_seen) :

                # get all instances from a single class
                class_instances = list(np.where(test_labels_seen == class_label)[0])

                # some assertion to check class names
                assert all(test_labels_seen[class_instances] == class_label), 'there is a problem in seen class search'

                test_class_image_batch = test_x_seen[class_instances, :, :, :]
                test_class_label_batch = test_labels_seen[class_instances]

                test_class_image_batch_float = np.array(test_class_image_batch).astype(np.float32)

                # Evaluate the features for the seen class instances
                [seen_class_mid_feats_] =\
                    self.sess.run([self.mid_feat_x_val],
                                  feed_dict={self.input_feat_x: test_class_image_batch_float})

                # Evaluate the full classifier accuracy on the seen class mid features
                [seen_class_mca_] = self.sess.run([self.full_val_acc],
                                                  feed_dict={self.classifier_feat_x: seen_class_mid_feats_,
                                                             self.image_labels: test_class_label_batch})
    
                seen_class_mca_vec[class_number] = seen_class_mca_

            assert all(seen_class_mca_vec > -0.00001)  # check if all classes have got some positive accuracy
            epoch_mca_test_seen = np.mean(seen_class_mca_vec)

            # -----------------------------------------------------------------
            # Validation function - Unseen val instances (Per class evaluation)
            # -----------------------------------------------------------------
            print('------------------------------------------------------------')
            print('ZSL : MCA Validation on epoch - Uneen Class Instances' + str(ep))
            print('------------------------------------------------------------')

            epoch_mca_test_unseen = 0.0
            unseen_class_mca_vec = -1*np.ones(len(test_classes_unseen))

            for class_number, class_label in enumerate(test_classes_unseen):

                # get all instances from a single class
                class_instances = list(np.where(test_labels_unseen == class_label)[0])

                # some assertion to check class names
                assert all(test_labels_unseen[class_instances] == class_label), 'there is a problem in unseen class search'

                test_class_image_batch = test_x_unseen[class_instances, :, :, :]
                test_class_label_batch = test_labels_unseen[class_instances]

                test_class_image_batch_float = np.array(test_class_image_batch).astype(np.float32)

                # Evaluate the features for the unseen class instances
                [unseen_class_mid_feats_] =\
                    self.sess.run([self.mid_feat_x_val],
                                  feed_dict={self.input_feat_x: test_class_image_batch_float})

                # Evaluate the full classifier accuracy on the unseen class mid features
                [unseen_class_mca_] = self.sess.run([self.full_val_acc],
                                                    feed_dict={self.classifier_feat_x: unseen_class_mid_feats_,
                                                               self.image_labels: test_class_label_batch})

                unseen_class_mca_vec[class_number] = unseen_class_mca_

            assert all(unseen_class_mca_vec > -0.00001)  # check if all classes have got some positive accuracy
            epoch_mca_test_unseen = np.mean(unseen_class_mca_vec)

            # Compute mean MCA and harmonic mean of seen and unseen MCA's
            epoch_harmonic_mca_test = 2*epoch_mca_test_unseen*epoch_mca_test_seen/(epoch_mca_test_unseen + epoch_mca_test_seen)
            epoch_mean_mca_test = np.mean(np.concatenate((unseen_class_mca_vec, seen_class_mca_vec), axis=0))

            # Summaries for MCA computations
            epoch_mca_test_unseen_summary = tf.Summary(value=[tf.Summary.Value(tag='zsl_mca/unseen', simple_value=epoch_mca_test_unseen)])
            epoch_mca_test_seen_summary = tf.Summary(value=[tf.Summary.Value(tag='zsl_mca/seen', simple_value=epoch_mca_test_seen)])

            epoch_mean_mca_test_summary = tf.Summary(value=[tf.Summary.Value(tag='zsl_mca/mean_of_mca_all',simple_value=epoch_mean_mca_test)])
            epoch_harmonic_mca_test_summary = tf.Summary(value=[tf.Summary.Value(tag='zsl_mca/harmonic_mean_of_seen_mca_and_unseen_mca',simple_value=epoch_harmonic_mca_test)])

            val_scalar_summaries_list = [epoch_mca_test_unseen_summary, epoch_mca_test_seen_summary,
                                         epoch_mean_mca_test_summary, epoch_harmonic_mca_test_summary]

            for temp_val_scalar_summary in val_scalar_summaries_list:
                self.writer_zsl_val.add_summary(temp_val_scalar_summary, ep)

            # -----------------------------------------------------------------
            # Training function : ZSL
            # -----------------------------------------------------------------

            # Iterate through the batches
            print('-----------------------------------')
            print('ZSL : Training on epoch ' + str(ep))
            print('-----------------------------------')
            batch_number = 0
            trained_sample_count = 0

            epoch_zsl_train_acc = 0.0
            epoch_zsl_train_loss = 0.0

            for batch_indices in batching(shuffled_train_indices, train_batch_size):

                # Skip small batches in the training
                if len(batch_indices) < train_batch_size:
                    print('important : skipped the last batch of size ' + str(len(batch_indices)))
                    continue

                batch_start_time = time.time()
                batch_number += 1
                sample_count = len(batch_indices) + len(repeated_classes_array_)  # include the generated training samples to the count
                total_count = trained_sample_count + sample_count

                # --------------------------------------------------------------
                # Step 1 : Generate features for the unseen classes
                # --------------------------------------------------------------

                [generated_unseen_batch_mid_feats_] =\
                    self.sess.run([self.gen_mid_feat_x_val],  # 1. Get mid features from attributes passed into the generator (Unseen classes)
                                  feed_dict={self.input_att_x: generator_input_attrib_array_})

                # -------------------------------------------------------------------------------
                # Step 2 : Sample the real image batch randomly
                # -------------------------------------------------------------------------------

                train_image_batch = train_x[batch_indices, :, :, :]
                train_label_batch = train_labels[batch_indices]

                train_image_batch_float = np.array(train_image_batch).astype(np.float32)

                # Evaluate the features for the seen batch instances
                [seen_batch_mid_feats_] =\
                    self.sess.run([self.mid_feat_x], # 1. Get the mid features from the training images from the seen classes
                                  feed_dict={self.input_feat_x: train_image_batch_float})

                # -------------------------------------------------------------------------------
                # Step 3 : Concatenate the real seen features with the unseen generated features
                # -------------------------------------------------------------------------------

                full_train_image_batch = np.concatenate((seen_batch_mid_feats_, generated_unseen_batch_mid_feats_), axis=0)
                full_train_labels_batch = np.concatenate((train_label_batch, repeated_classes_array_), axis=0)

                # --------------------------------------------------------------
                # Step 4 : Train the full class classifier
                # --------------------------------------------------------------

                _, zsl_train_acc_, zsl_train_loss_,  =\
                    self.sess.run([full_cls_train_step,   # 1. Full classifier train step
                                  self.full_train_acc,    # 2. Full classifier train accuracy
                                  self.full_train_loss],  # 3. Full classifier train loss
                                  feed_dict={self.classifier_feat_x: full_train_image_batch,
                                             self.image_labels: full_train_labels_batch})

                epoch_zsl_train_acc = (zsl_train_acc_*sample_count + epoch_zsl_train_acc*trained_sample_count)/total_count
                epoch_zsl_train_loss = (zsl_train_loss_*sample_count + epoch_zsl_train_loss*trained_sample_count)/total_count

                trained_sample_count = total_count + 0  # Added zero to avoid any mutative assignments

                if batch_number % self.train_params['display_interval'] == 0:
                        print('zsl seen - train epoch :' + str(ep) + ', processing batch :' + str(batch_number) +
                              ', in ' + str(round(time.time()-batch_start_time, 4)) + 's ' +
                              ', train data acc  : ' + str(round(epoch_zsl_train_acc, 4)) +
                              ', train data loss : ' + str(round(epoch_zsl_train_loss, 4)))

            epoch_zsl_train_acc_summary = tf.Summary(value=[tf.Summary.Value(tag='zsl_classification_accuracies/train_acc',simple_value=epoch_zsl_train_acc)])
            epoch_zsl_train_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='zsl_classification_losses/train_loss',simple_value=epoch_zsl_train_loss)])

            train_scalar_summaries_list = [epoch_zsl_train_acc_summary, epoch_zsl_train_loss_summary]

            for temp_train_scalar_summary in train_scalar_summaries_list:
                self.writer_zsl_train.add_summary(temp_train_scalar_summary, ep)

            # ------------------------------------------------------------------
            # Save dictionaries for the embeddings
            # ------------------------------------------------------------------

            embeddings = {}
            embeddings_real = {}
            embeddings_gen = {}

            embeddings_real_x = np.array([])
            embeddings_gen_x = np.array([])

            embeddings_real_y = list()
            embeddings_gen_y = list()

            # -----------------------------------------------------------------
            # Training function : ZSL
            # -----------------------------------------------------------------

            # Iterate through the batches
            print('-----------------------------------------')
            print('ZSL : Extracting embeddings on ' + str(ep))
            print('-----------------------------------------')
            batch_number = 0

            for batch_indices in batching(shuffled_unseen_val_indices, val_batch_size):

                batch_number += 1

                # -------------------------------------------------------------
                # Step 1 : Generate features for the unseen classes
                # -------------------------------------------------------------

                # Changed to seen classes
                [generated_batch_mid_feats_] =\
                    self.sess.run([self.gen_mid_feat_x_val],  # 1. Get mid features from attributes passed into the generator (Seen classes)
                                  feed_dict={self.input_att_x: generator_input_attrib_array_})

                # -------------------------------------------------------------
                # Step 2 : Sample the real image batch randomly
                # -------------------------------------------------------------

                unseen_test_image_batch = test_x_unseen[batch_indices, :, :, :]
                unseen_test_label_batch = test_labels_unseen[batch_indices]

                unseen_test_image_batch_float = np.array(unseen_test_image_batch).astype(np.float32)

                # Evaluate the features for the seen batch instances
                [unseen_batch_mid_feats_] =\
                    self.sess.run([self.mid_feat_x_val],  # 1. Get the mid features from the training images from the seen classes
                                  feed_dict={self.input_feat_x: unseen_test_image_batch_float})

                # Stack the feature embeddings
                if batch_number == 1:
                    # copy the features
                    embeddings_real_x = np.copy(unseen_batch_mid_feats_)
                    embeddings_gen_x = np.copy(generated_batch_mid_feats_)

                else:
                    # concatenate the features
                    embeddings_real_x = np.concatenate((embeddings_real_x, unseen_batch_mid_feats_), axis=0)
                    embeddings_gen_x = np.concatenate((embeddings_gen_x, generated_batch_mid_feats_), axis=0)

                # Stack the labels
                embeddings_real_y.extend(unseen_test_label_batch)
                embeddings_gen_y.extend(generator_input_labels_list_)

            # -----------------------------------------------------------------
            # Convert and save the features
            # -----------------------------------------------------------------
            embedding_file_name = self.train_params['embeddings_dir'] + '/' + self.train_params['dataset_name'] + '_' + self.train_params['exp_title']

            # some conversions of the labels list to a numpy array
            embeddings_real_y_array = np.array(embeddings_real_y)
            embeddings_gen_y_array = np.array(embeddings_gen_y)

            # some assertions
            assert embeddings_real_x.shape[0] == len(unseen_val_indices_array), 'the number of real samples are different'
            assert embeddings_real_x.shape[0] == embeddings_real_y_array.size, 'the number of real samples are different'
            assert embeddings_gen_x.shape[0] == embeddings_gen_y_array.size, 'the number of generated samples are different'

            assert embeddings_real_x.shape[3] == self.train_params['classifier_feat_dim']
            assert embeddings_gen_x.shape[3] == self.train_params['classifier_feat_dim']

            # put the embedding to a dictionary
            embeddings_real = {'x': embeddings_real_x, 'y': embeddings_real_y_array}
            embeddings_gen = {'x': embeddings_gen_x, 'y': embeddings_gen_y_array}

            embeddings = {'real': embeddings_real, 'gen': embeddings_gen}

            # save the embeddings model
            scipy.io.savemat(embedding_file_name, embeddings)

        # Close the file writers
        self.writer_zsl_train.close()
        self.writer_zsl_val.close()

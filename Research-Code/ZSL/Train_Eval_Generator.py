#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 17:00:35 2018

@author: her171
"""

from __future__ import division
import numpy as np
import time
import random

import tensorflow as tf
from tensorflow.python import debug as tf_debug
slim = tf.contrib.slim


class Train_Eval_Generator:

    # --------------------------------------------------------------------------
    # GAN - Training Opts defining and Training and Evaluation Method
    # --------------------------------------------------------------------------
    def train_eval_generator(self):

        # fix the random seed for the experiment-------------------------------
        tf.set_random_seed(0)
        np.random.seed(0)
        random.seed(0)
        # ---------------------------------------------------------------------

        if self.train_params['debug_mode']:
            self.sess = tf_debug.LocalCLIDebugWrapperSession(self.sess)
            self.sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)

        with tf.variable_scope('optimizers_and_train_steps'):

            optimizer_generator = tf.train.RMSPropOptimizer(self.train_params['learning_rate_gan'], name='optimizer_generator')
            optimizer_seen_class = tf.train.RMSPropOptimizer(self.train_params['learning_rate_gan'], name='optimizer_seen_class')

        # ---------------------------------------------------------------------
        # Define the train ops
        # ---------------------------------------------------------------------

            # -----------------------------------------------------------------
            # Feature Extractor Train Ops
            # -----------------------------------------------------------------
            feat_ext_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope=self.train_params['feature_extractor_update_op_scope'])
            print(' ')
            print('============== feat extractor update ops =================')
            for uop in feat_ext_update_ops:
                print(uop.op.name)
            print('==========================================================')

            # -----------------------------------------------------------------
            # Seen Class Classifier Train Ops
            # -----------------------------------------------------------------

            seen_cls_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope=self.train_params['seen_classifier_model_update_op_scope'])
            print(' ')
            print('============== seen classifier update ops ================')
            for uop in seen_cls_update_ops:
                print(uop.op.name)
            print('==========================================================')

            seen_cls_global_step = tf.get_variable('seen_cls_global_step', [], initializer=tf.constant_initializer(0.0), trainable=False)
            seen_cls_train_step = slim.learning.create_train_op(self.feature_extractor_train_loss, 
                                                                optimizer_seen_class,
                                                                variables_to_train=self.seen_classifier_variables + self.feature_extractor_variables,
                                                                global_step=seen_cls_global_step,
                                                                update_ops=seen_cls_update_ops + feat_ext_update_ops)

            # ------------------------------------------------------------------
            # Generator Train Ops
            # ------------------------------------------------------------------
            sam_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope=self.train_params['sam_update_op_scope'])
            generator_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope=self.train_params['generator_model_update_op_scope'])
            full_generator_update_ops = generator_update_ops + sam_update_ops
            full_generator_train_variables = self.generator_function_variables + self.sam_variables

            print(' ')
            print('================ full generator update ops ===============')
            for uop in full_generator_update_ops:
                print(uop.op.name)
            print('==========================================================')

            generator_global_step = tf.get_variable('generator_global_step', [], initializer=tf.constant_initializer(0.0), trainable=False)
            generator_train_step = slim.learning.create_train_op(self.generator_train_loss, 
                                                                 optimizer_generator,
                                                                 variables_to_train=full_generator_train_variables,
                                                                 global_step=generator_global_step,
                                                                 update_ops=full_generator_update_ops)

        # ----------------------------------------------------------------------
        # Load features and attribute data
        # ----------------------------------------------------------------------
        # load the re-labeled attribute matrix and the label matrix for the GAN training
        att_x = self.data_object.gan_stage_att
        train_x = self.data_object.gan_stage_train_feats['x']
        train_labels = self.data_object.gan_stage_train_feats['y']

        test_x_seen = self.data_object.gan_stage_test_feats['x']
        test_labels_seen = self.data_object.gan_stage_test_feats['y']

        # ---------------------------------------------------------------------
        # Decide the training duration
        # ---------------------------------------------------------------------
        number_of_epochs = self.train_params['number_of_epochs']
        train_batch_size = self.train_params['train_batch_size']
        val_batch_size = self.train_params['val_batch_size']

        # Load image paths and labels
        train_indices_array = np.arange(len(train_labels))
        val_indices_array = np.arange(len(test_labels_seen))

        # ---------------------------------------------------------------------
        # Initialize the model 
        # ---------------------------------------------------------------------
        self.sess.run(tf.local_variables_initializer())
        self.sess.run(tf.global_variables_initializer())

        # ---------------------------------------------------------------------
        # Initialize the summary writer
        # ---------------------------------------------------------------------
        self.writer_gan_train = tf.summary.FileWriter(self.train_params['summaries_dir']+'/gan_train', self.sess.graph)
        self.writer_gan_val = tf.summary.FileWriter(self.train_params['summaries_dir']+'/gan_val', self.sess.graph)
        # self.writer_grad = tf.summary.FileWriter(self.train_params['summaries_dir']+'/grad', self.sess.graph)

        # Iterate through the epochs
        for ep in range(number_of_epochs):

            # Batching function
            def batching(samples, batch_size):
                return (samples[pos:pos+batch_size] for pos in xrange(0, len(samples), batch_size))

            np.random.shuffle(val_indices_array)
            np.random.shuffle(train_indices_array)

            shuffled_val_indices = list(val_indices_array)
            shuffled_train_indices = list(train_indices_array)

            # ------------------------------------------------------------------
            # Validation function
            # ------------------------------------------------------------------

            # Iterate through the batches
            print('-----------------------------')
            print('validation on epoch ' + str(ep))
            print('-----------------------------')
            batch_number = 0
            evaluated_sample_count = 0

            epoch_real_seen_val_acc, epoch_gen_seen_val_acc = 0.0, 0.0
            epoch_real_seen_val_loss, epoch_gen_seen_val_loss = 0.0, 0.0

            for batch_indices in batching(shuffled_val_indices, val_batch_size):

                batch_start_time = time.time()
                batch_number += 1
                sample_count = len(batch_indices)
                total_count = evaluated_sample_count + sample_count

                test_image_batch = test_x_seen[batch_indices, :, :, :]
                test_label_batch = test_labels_seen[batch_indices]
                test_att_batch = att_x[test_label_batch, :, :, :]

                test_image_batch_float = np.array(test_image_batch).astype(np.float32)
                test_att_batch_float = np.array(test_att_batch).astype(np.float32)

                #      1                    2                  3                  4
                real_seen_val_acc_, gen_seen_val_acc_, real_seen_val_loss_, gen_seen_val_loss_ =\
                    self.sess.run([self.real_seen_val_acc,  # 1. Classification accuracy for real samples from seen classes
                                  self.gen_seen_val_acc,    # 2. Classification accuracy for generated samples from seen classes
                                  self.real_seen_val_loss,  # 3. Classification loss for real samples from seen classes
                                  self.gen_seen_val_loss],  # 4. Classification loss for generated samples from the seen classes
                                  feed_dict={self.input_feat_x: test_image_batch_float,
                                             self.input_att_x: test_att_batch_float,
                                             self.image_labels: test_label_batch})

                epoch_real_seen_val_acc = (real_seen_val_acc_*sample_count + epoch_real_seen_val_acc*evaluated_sample_count)/total_count
                epoch_gen_seen_val_acc = (gen_seen_val_acc_*sample_count + epoch_gen_seen_val_acc*evaluated_sample_count)/total_count

                epoch_real_seen_val_loss = (real_seen_val_loss_*sample_count + epoch_real_seen_val_loss*evaluated_sample_count)/total_count
                epoch_gen_seen_val_loss = (gen_seen_val_loss_*sample_count + epoch_gen_seen_val_loss*evaluated_sample_count)/total_count

                evaluated_sample_count = total_count + 0  # Added zero to avoid any mutative assignments

                if batch_number % self.train_params['display_interval'] == 0:
                        print('val epoch :' + str(ep) + ', processing batch :' + str(batch_number) +
                              ', in ' + str(round(time.time()-batch_start_time, 4)) + 's ' +
                              ', real data acc  : ' + str(round(epoch_real_seen_val_acc, 4)) +
                              ', real data loss : ' + str(round(epoch_real_seen_val_loss, 4)) +
                              ', gen data acc  : ' + str(round(epoch_gen_seen_val_acc, 4)) +
                              ', gen data loss : ' + str(round(epoch_gen_seen_val_loss, 4)))
                
                if sample_count < val_batch_size:
                    print('Evaluated a smaller batch : the batch size in batch ' + str(batch_number) + ' is ' + str(sample_count))

            epoch_real_seen_val_acc_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_accuracies/real_seen', simple_value=epoch_real_seen_val_acc)])
            epoch_gen_seen_val_acc_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_accuracies/generated_seen', simple_value=epoch_gen_seen_val_acc)])

            epoch_real_seen_val_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_losses/real_seen', simple_value=epoch_real_seen_val_loss)])
            epoch_gen_seen_val_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_losses/generated_seen', simple_value=epoch_gen_seen_val_loss)])

            val_scalar_summaries_list = [epoch_real_seen_val_acc_summary,  epoch_gen_seen_val_acc_summary,
                                         epoch_real_seen_val_loss_summary, epoch_gen_seen_val_loss_summary]

            for temp_val_scalar_summary in val_scalar_summaries_list:
                self.writer_gan_val.add_summary(temp_val_scalar_summary, ep)

            # -----------------------------------------------------------------
            # Training function
            # -----------------------------------------------------------------

            # Iterate through the batches
            print('-----------------------------')
            print('training on epoch ' + str(ep))
            print('-----------------------------')
            batch_number = 0
            trained_sample_count = 0

            epoch_generator_train_loss = 0.0

            epoch_real_seen_train_acc, epoch_gen_seen_train_acc = 0.0, 0.0
            epoch_real_seen_train_loss, epoch_gen_seen_train_loss = 0.0, 0.0
            epoch_stat_align_loss = 0.0

            for batch_indices in batching(shuffled_train_indices, train_batch_size):

                # Skip small batches in the training
                if len(batch_indices) < train_batch_size:
                    print('important : skipped the last batch of size ' + str(len(batch_indices)))
                    continue

                batch_start_time = time.time()
                batch_number += 1
                sample_count = len(batch_indices)
                total_count = trained_sample_count + sample_count

                train_image_batch = train_x[batch_indices, :, :, :]
                train_label_batch = train_labels[batch_indices]
                train_att_batch = att_x[train_label_batch, :, :, :]

                train_image_batch_float = np.array(train_image_batch).astype(np.float32)
                train_att_batch_float = np.array(train_att_batch).astype(np.float32)

                # -------------------------------------------------------------
                # Train step for the feature extractor
                # --------------------------------------------------------------

                _, real_seen_train_loss_, real_seen_train_acc_ =\
                    self.sess.run([seen_cls_train_step,
                                  self.real_seen_train_loss,  # 1. Classifier training on real seen train samples
                                  self.real_seen_train_acc],  # 2. Classifier accuracy on real seen train samples
                                  feed_dict={self.input_feat_x: train_image_batch_float,
                                             self.image_labels: train_label_batch})

                # --------------------------------------------------------------
                # Decide when to start the confused/unconfused statistics 
                # matching and what form of statistics (confused or uncconfused)
                # are matched at each iteration.
                # --------------------------------------------------------------

                if ep >= self.train_params['sam_train_delay']:
                    gamma_gen_cls_ = 1.0
                    gamma_sam_ = 1.0

                else:
                    gamma_gen_cls_ = 1.0
                    gamma_sam_ = 0.0

                # -------------------------------------------------------------
                # Generator train step and loss computations
                # -------------------------------------------------------------
                # 1,      2                     3                   4
                _,  gen_seen_train_loss_, gen_seen_train_acc_, stat_align_loss_ =\
                    self.sess.run([generator_train_step,          # 1. Generator train step (minimize only the switeched on loss terms by gamma swithces)
                                  self.gen_seen_train_loss,       # 2. Generated data from seen classes classification loss
                                  self.gen_seen_train_acc,        # 3. Generated data from seen classes classification accuracy
                                  self.stat_align_loss],          # 4. stat align loss from statistical alignment module
                                  feed_dict={self.input_feat_x: train_image_batch_float,
                                             self.input_att_x: train_att_batch_float,
                                             self.image_labels: train_label_batch,
                                             self.gamma_gen_cls: gamma_gen_cls_,
                                             self.gamma_sam: gamma_sam_})

                # -------------------------------------------------------------
                # Update SAM module moments accumulation variables
                # -------------------------------------------------------------
                # 1
                if ep >= self.train_params['sam_train_delay']:
                    _ = \
                        self.sess.run([self.update_moments],  # Updated the moments accumulation variables in the SAM module
                                      feed_dict={self.input_feat_x: train_image_batch_float,
                                                 self.input_att_x: train_att_batch_float})

                # --------------------------------------------------------------
                # Compute some losses 
                # --------------------------------------------------------------
                [gamma_gen_cls_, gamma_sam_] = [1.0, 1.0]
                # 1
                [generator_train_loss_] = \
                    self.sess.run([self.generator_train_loss],  # 1. Generator's training loss with all the loss terms switched on
                                  feed_dict={self.input_feat_x: train_image_batch_float,
                                             self.input_att_x: train_att_batch_float,
                                             self.image_labels: train_label_batch,
                                             self.gamma_gen_cls: gamma_gen_cls_,
                                             self.gamma_sam: gamma_sam_})

                epoch_generator_train_loss = (generator_train_loss_*sample_count + epoch_generator_train_loss*trained_sample_count)/total_count
                epoch_real_seen_train_loss = (real_seen_train_loss_*sample_count + epoch_real_seen_train_loss*trained_sample_count)/total_count
                epoch_gen_seen_train_loss = (gen_seen_train_loss_*sample_count + epoch_gen_seen_train_loss*trained_sample_count)/total_count

                epoch_stat_align_loss = (stat_align_loss_*sample_count + epoch_stat_align_loss*trained_sample_count)/total_count

                epoch_real_seen_train_acc = (real_seen_train_acc_*sample_count + epoch_real_seen_train_acc*trained_sample_count)/total_count
                epoch_gen_seen_train_acc = (gen_seen_train_acc_*sample_count + epoch_gen_seen_train_acc*trained_sample_count)/total_count

                trained_sample_count = total_count + 0  # Added zero to avoid any mutative assignments

                if batch_number % self.train_params['display_interval'] == 0:
                        print('train epoch :' + str(ep) + ', processing batch :' + str(batch_number) +
                              ', in ' + str(round(time.time()-batch_start_time, 4)) + 's ' +
                              ', real data acc  : ' + str(round(epoch_real_seen_train_acc, 4)) +
                              ', real data loss : ' + str(round(epoch_real_seen_train_loss, 4)) +
                              ', gen data acc  : ' + str(round(epoch_gen_seen_train_acc, 4)) +
                              ', gen data loss : ' + str(round(epoch_gen_seen_train_loss, 4)))

            epoch_real_seen_train_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_losses/real_seen', simple_value=epoch_real_seen_train_loss)])
            epoch_gen_seen_train_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_losses/generated_seen', simple_value=epoch_gen_seen_train_loss)])

            epoch_generator_train_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='GAN_losses/generator_train_loss', simple_value=epoch_generator_train_loss)])  

            epoch_real_seen_train_acc_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_accuracies/real_seen', simple_value=epoch_real_seen_train_acc)])
            epoch_gen_seen_train_acc_summary = tf.Summary(value=[tf.Summary.Value(tag='classification_accuracies/generated_seen', simple_value=epoch_gen_seen_train_acc)])

            epoch_stat_align_loss_summary = tf.Summary(value=[tf.Summary.Value(tag='stats_loss/stat_loss', simple_value=epoch_stat_align_loss)])

            epoch_stat_loss_weight_summary = tf.Summary(value=[tf.Summary.Value(tag='parameters/sam_alpha_weight', simple_value=self.train_params['alpha_weight_sam'])])
            epoch_tgt_cls_loss_weight_summary = tf.Summary(value=[tf.Summary.Value(tag='parameters/generated_cls_loss_alpha_weight', simple_value=self.train_params['alpha_weight_gen_cls'])])

            train_scalar_summaries_list = [epoch_real_seen_train_loss_summary, epoch_gen_seen_train_loss_summary, 
                                           epoch_generator_train_loss_summary,
                                           epoch_real_seen_train_acc_summary, epoch_gen_seen_train_acc_summary,
                                           epoch_stat_align_loss_summary,
                                           epoch_stat_loss_weight_summary, epoch_tgt_cls_loss_weight_summary]

            for temp_train_scalar_summary in train_scalar_summaries_list:
                self.writer_gan_train.add_summary(temp_train_scalar_summary, ep)

            # -----------------------------------------------------------------
            # Save Checkpoint
            # -----------------------------------------------------------------
            if self.train_params['save_checkpoints']:
                self.save_checkpoint(self.train_params['checkpoint_prefix'], ep)

        # Close the file writers 
        self.writer_gan_train.close()
        self.writer_gan_val.close()

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Run this code to train and eval the network
"""

import os
import sys
import argparse
import tensorflow as tf
import Siamese
from load_datasets import get_data

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


def main(_):

    # ==========================================================================
    # Argument collector
    # ==========================================================================

    parser = argparse.ArgumentParser(description='Input arguments to get different training models',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # --------------------------------------------------------------------------
    # Experiment and Dataset Parameters
    # --------------------------------------------------------------------------
    parser.add_argument('--exp_title', type=str, default='plain_confusion_ft_dim_1024')  # min_with_manual_accum_momentum_0p0
    parser.add_argument('--exp_id', type=int, default=2)

    parser.add_argument('--dataset_folder', type=str, default='/flush3/her171/ZSL-GAN/Data/Data-gbu')
    parser.add_argument('--dataset_name', type=str, default='Sun')

    # --------------------------------------------------------------------------
    # SAM parameters
    # --------------------------------------------------------------------------
    parser.add_argument('--sam_use_accumulate_moments', action='store_true')
    parser.add_argument('--sam_momentum', type=float, default=0.5)  # Accumulation momentum of statistics
    parser.add_argument('--sam_use_diag', action='store_true')  # Use diagonal covariance matrix
    parser.add_argument('--sam_functionality', type=str, default='min_max')  # 'min', 'min_max' or 'coral'
    parser.add_argument('--sam_train_delay', type=int, default=100)

    # Confusion function parameters
    parser.add_argument('--conf_num_layers', type=int, default=2)  # number of layers in the conf model
    parser.add_argument('--conf_mid_activation', type=str, default='leaky_relu')  # Mid layer activations of the conf layer
    parser.add_argument('--conf_mid_layer_dim', type=int, default=64)  # Mid layer dimensions of the conf model
    parser.add_argument('--conf_use_bnorm', action='store_true')  # Use batch normalization in conf model

    # --------------------------------------------------------------------------
    # Loss parameters 
    # --------------------------------------------------------------------------
    parser.add_argument('--alpha_weight_sam', type=float, default=0.1)  # KL loss weight confised(adverserial) Reduce this by 0.1
    parser.add_argument('--alpha_weight_gen_cls', type=float, default=0.1)  # Loss weight for classifying the generated data

    # --------------------------------------------------------------------------
    # Training Parameters
    # --------------------------------------------------------------------------
    parser.add_argument('--sampling_rate', type=float, default=1.0)
    parser.add_argument('--start_gen_classification_epoch', type=int, default=-1)  # Not implemented

    parser.add_argument('--solver_type', type=str, default='rmsp')
    parser.add_argument('--learning_rate_gan', type=float, default=0.0001)
    parser.add_argument('--learning_rate_zsl', type=float, default=0.0001)

    parser.add_argument('--train_batch_size', type=int, default=1024)  # TODO : changed from 1024 for a experiment
    parser.add_argument('--val_batch_size', type=int, default=128)
    parser.add_argument('--number_of_epochs', type=int, default=500)
    parser.add_argument('--number_of_zsl_epochs', type=int, default=2000)

    parser.add_argument('--display_interval', type=int, default=100)

    # --------------------------------------------------------------------------
    # Model Parameters
    # --------------------------------------------------------------------------
    parser.add_argument('--classifier_feat_dim', type=int, default=1024)
    parser.add_argument('--confused_feat_dim', type=int, default=1024)
    parser.add_argument('--use_compact_feats', action='store_true')

    parser.add_argument('--classifier_model', type=str, default='classifier_function')
    parser.add_argument('--feature_extractor', type=str, default='feature_extractor_function')
    parser.add_argument('--generator_model', type=str, default='generator_function')
    parser.add_argument('--confusion_model', type=str, default='plain_confusion')

    # Training and Loading Variables
    parser.add_argument('--excluded_loading_variables', type=str, default="['Adam','Momentum']")
    parser.add_argument('--excluded_training_variables', type=str, default="['Adam','Momentum','moving_mean','moving_variance','moving_mean_vec','moving_cov_mat']")
    parser.add_argument('--seen_classifier_model_update_op_scope', type=str, default="seen_classifier_function/fully_connected_classifier")
    parser.add_argument('--full_classifier_model_update_op_scope', type=str, default="full_classifier_function/fully_connected_classifier")
    parser.add_argument('--feature_extractor_update_op_scope',type=str, default="feature_extractor_function/fully_connected_feature_extractor")
    parser.add_argument('--generator_model_update_op_scope', type=str, default="generator_function/fully_connected_generator")
    parser.add_argument('--sam_update_op_scope', type=str, default="statistical_alignment_module/stat_align_module")

    # Checkpoint/Summaries saving details
    parser.add_argument('--save_checkpoints', type=bool, default=True)
    parser.add_argument('--log_dir', type=str, default='/flush3/her171/Interactive-CVPR19/Zero-Shot-Learning/SAM-Sampling-Change')
    parser.add_argument('--checkpoint_prefix', type=str, default='zsl-gan')

    # --------------------------------------------------------------------------
    # Running the debugger
    # --------------------------------------------------------------------------
    parser.add_argument('--debug_mode', action='store_true')

    argument_inputs = parser.parse_args()

    # ==========================================================================
    # Parsing the input arguments
    # ==========================================================================

    SETUP_PARAMS = {}  # Main dictionary to save the setup parameters
    TRAIN_PARAMS = {}  # Training parameter dictionary

    # --------------------------------------------------------------------------
    # Experiment and Dataset Parameters
    # --------------------------------------------------------------------------

    TRAIN_PARAMS['dataset_name'] = argument_inputs.dataset_name
    TRAIN_PARAMS['exp_title'] = argument_inputs.exp_title

    num_of_attributes_dict = {'Awa2': 85, 'Awa1': 85,  'Cub': 312, 'Sun': 102}
    num_seen_classes_dict = {'Awa2': 40, 'Awa1': 40,  'Cub': 150, 'Sun': 645}
    num_unseen_classes_dict = {'Awa2': 10, 'Awa1': 10,  'Cub': 50, 'Sun': 72}
        
    TRAIN_PARAMS['num_attributes'] = num_of_attributes_dict[argument_inputs.dataset_name]
    TRAIN_PARAMS['num_seen_classes'] = num_seen_classes_dict[argument_inputs.dataset_name]
    TRAIN_PARAMS['num_unseen_classes'] = num_unseen_classes_dict[argument_inputs.dataset_name]
    
    TRAIN_PARAMS['classifier_feat_dim'] = argument_inputs.classifier_feat_dim
    TRAIN_PARAMS['confused_feat_dim'] = argument_inputs.confused_feat_dim
    TRAIN_PARAMS['use_compact_feats'] = argument_inputs.use_compact_feats

    # --------------------------------------------------------------------------
    # SAM parameters
    # --------------------------------------------------------------------------
    TRAIN_PARAMS['sam_use_accumulate_moments'] = argument_inputs.sam_use_accumulate_moments
    TRAIN_PARAMS['sam_momentum'] = argument_inputs.sam_momentum
    TRAIN_PARAMS['sam_use_diag'] = argument_inputs.sam_use_diag
    TRAIN_PARAMS['sam_functionality'] = argument_inputs.sam_functionality
    TRAIN_PARAMS['sam_train_delay'] = argument_inputs.sam_train_delay

    # Confusion function parameters
    TRAIN_PARAMS['conf_num_layers'] = argument_inputs.conf_num_layers
    TRAIN_PARAMS['conf_mid_activation'] = argument_inputs.conf_mid_activation
    TRAIN_PARAMS['conf_mid_layer_dim'] = argument_inputs.conf_mid_layer_dim
    TRAIN_PARAMS['conf_use_bnorm'] = argument_inputs.conf_use_bnorm

    # --------------------------------------------------------------------------
    # Model Parameters
    # --------------------------------------------------------------------------

    TRAIN_PARAMS['classifier_model'] = argument_inputs.classifier_model
    TRAIN_PARAMS['feature_extractor'] = argument_inputs.feature_extractor
    TRAIN_PARAMS['generator_model'] = argument_inputs.generator_model
    TRAIN_PARAMS['confusion_model'] = argument_inputs.confusion_model

    TRAIN_PARAMS['excluded_loading_variables'] = eval(argument_inputs.excluded_loading_variables)
    TRAIN_PARAMS['excluded_training_variables'] = eval(argument_inputs.excluded_training_variables)

    TRAIN_PARAMS['seen_classifier_model_update_op_scope'] = argument_inputs.seen_classifier_model_update_op_scope
    TRAIN_PARAMS['full_classifier_model_update_op_scope'] = argument_inputs.full_classifier_model_update_op_scope
    TRAIN_PARAMS['feature_extractor_update_op_scope'] = argument_inputs.feature_extractor_update_op_scope 
    TRAIN_PARAMS['generator_model_update_op_scope'] = argument_inputs.generator_model_update_op_scope
    TRAIN_PARAMS['sam_update_op_scope'] = argument_inputs.sam_update_op_scope

    # --------------------------------------------------------------------------
    # Loss parameters
    # --------------------------------------------------------------------------
    TRAIN_PARAMS['alpha_weight_sam'] = argument_inputs.alpha_weight_sam
    TRAIN_PARAMS['alpha_weight_gen_cls'] = argument_inputs.alpha_weight_gen_cls

    # --------------------------------------------------------------------------
    # Training Parameters
    # --------------------------------------------------------------------------

    # Confusion Starting and weight parameters
    TRAIN_PARAMS['sampling_rate'] = argument_inputs.sampling_rate

    TRAIN_PARAMS['start_gen_classification_epoch'] = argument_inputs.start_gen_classification_epoch

    TRAIN_PARAMS['solver_type'] = argument_inputs.solver_type
    TRAIN_PARAMS['learning_rate_gan'] = argument_inputs.learning_rate_gan
    TRAIN_PARAMS['learning_rate_zsl'] = argument_inputs.learning_rate_zsl

    TRAIN_PARAMS['train_batch_size'] = argument_inputs.train_batch_size
    TRAIN_PARAMS['number_of_epochs'] = argument_inputs.number_of_epochs
    TRAIN_PARAMS['number_of_zsl_epochs'] = argument_inputs.number_of_zsl_epochs

    # --------------------------------------------------------------------------
    # Logging and evaluation parameters
    # --------------------------------------------------------------------------

    full_exp_title = argument_inputs.dataset_name + '_' + argument_inputs.exp_title + '_' + str(argument_inputs.exp_id)

    TRAIN_PARAMS['log_dir'] = argument_inputs.log_dir
    TRAIN_PARAMS['embeddings_dir'] = argument_inputs.log_dir + '/embeddings'    
    TRAIN_PARAMS['summaries_dir'] = argument_inputs.log_dir + '/summaries/' + full_exp_title

    if not os.path.exists(TRAIN_PARAMS['summaries_dir']):
        os.makedirs(TRAIN_PARAMS['summaries_dir']+'/gan_train')
        os.makedirs(TRAIN_PARAMS['summaries_dir']+'/gan_val')
        os.makedirs(TRAIN_PARAMS['summaries_dir']+'/zsl_train')
        os.makedirs(TRAIN_PARAMS['summaries_dir']+'/zsl_val')

    if not os.path.exists(TRAIN_PARAMS['embeddings_dir']):
        os.makedirs(TRAIN_PARAMS['embeddings_dir'])

    TRAIN_PARAMS['checkpoint_dir'] = argument_inputs.log_dir + '/checkpoints/' + full_exp_title

    if not os.path.exists(TRAIN_PARAMS['checkpoint_dir']):
        os.makedirs(TRAIN_PARAMS['checkpoint_dir'])

    TRAIN_PARAMS['save_checkpoints'] = argument_inputs.save_checkpoints
    TRAIN_PARAMS['checkpoint_prefix'] = argument_inputs.checkpoint_prefix
    TRAIN_PARAMS['val_batch_size'] = argument_inputs.val_batch_size
    TRAIN_PARAMS['display_interval'] = argument_inputs.display_interval

    # --------------------------------------------------------------------------
    # Run the debugger
    # --------------------------------------------------------------------------
    TRAIN_PARAMS['debug_mode'] = argument_inputs.debug_mode

    SETUP_PARAMS['train_params'] = TRAIN_PARAMS

    # ==========================================================================
    # Train and Eval the model
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Grab data from the datasets
    # --------------------------------------------------------------------------
    data_object = get_data(argument_inputs.dataset_name, argument_inputs.dataset_folder)  # Returns an object for the dataset

    # --------------------------------------------------------------------------
    # Run the code
    # --------------------------------------------------------------------------
    run_config = tf.ConfigProto()
    run_config.gpu_options.allow_growth = True
    run_config.gpu_options.per_process_gpu_memory_fraction = 1

    print(SETUP_PARAMS['train_params'])
    tf.set_random_seed(0)

    # os.environ["TF_USE_CUDNN"]="1"
    with tf.Session(config=run_config) as sess:

        siamese = Siamese.Siamese(sess,
                                  data_object=data_object,
                                  SETUP_PARAMS=SETUP_PARAMS)

    # call the train_eval method to train and evaluate the model
    siamese.train_eval_generator()
    siamese.train_eval_zsl()


if __name__ == '__main__':
    tf.app.run()

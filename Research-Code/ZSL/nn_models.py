#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Define any discriminator functions used here. The discriminator arg_scope is too defined here
@author: her171
"""

from __future__ import division
import tensorflow as tf

from extra_layers import leaky_relu, noise
from tensorflow.contrib.framework import add_arg_scope

slim = tf.contrib.slim


# ------------------------------------------------------------------------------
# Network ArgScope
# ------------------------------------------------------------------------------
def network_arg_scope():
    with slim.arg_scope([slim.fully_connected, slim.conv2d],
                        weights_regularizer=slim.l2_regularizer(0.00001)) as network_arg_scope:
        return network_arg_scope


# ------------------------------------------------------------------------------
# Classifier function
# ------------------------------------------------------------------------------

def classifier_function(inputs,
                        is_training=True,
                        reuse=False,
                        num_classes=40):
    print('------------------------------------------------------------------')
    print('classifier function defined')
    print('------------------------------------------------------------------')

    with tf.variable_scope('fully_connected_classifier', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.0):
            with slim.arg_scope([slim.dropout], is_training=is_training):
                with slim.arg_scope([slim.batch_norm], decay=0.9, is_training=is_training, scale=True):
                    with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu,
                                        normalizer_fn=slim.batch_norm):
                        # net = slim.fully_connected(inputs, 256, scope = 'cl_fc1')
                        # net = slim.dropout(net, 0.5, is_training=is_training, scope='cl_dropout1')
                        net = slim.fully_connected(inputs, num_classes, activation_fn=None, normalizer_fn=None,
                                                   scope='cl_fc2')

                        logits = tf.squeeze(net, name='SpatialSqueeze')

                        return logits


# ------------------------------------------------------------------------------
# Feature extraction function
# ------------------------------------------------------------------------------
def feature_extractor_function(inputs,
                               is_training=True,
                               reuse=False,
                               output_dim=128):

    print('------------------------------------------------------------------')
    print('feature extractor function defined')
    print('------------------------------------------------------------------')

    with tf.variable_scope('fully_connected_feature_extractor', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.1):
            with slim.arg_scope([slim.dropout], is_training=is_training):
                with slim.arg_scope([slim.batch_norm], decay=0.9, is_training=is_training, scale=True):
                    with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu,
                                        normalizer_fn=slim.batch_norm):

                        # net  = slim.fully_connected(inputs, 1024, scope = 'fe_fc1')
                        # net  = slim.fully_connected(net, 1024, scope = 'fe_fc2')
                        # net  = slim.fully_connected(net, output_dim, scope = 'fe_fc3')

                        net = slim.fully_connected(inputs, output_dim, scope='fe_fc3')

                        return net


# ------------------------------------------------------------------------------
# Generator function
# ------------------------------------------------------------------------------
def generator_function(inputs,
                       is_training=True,
                       reuse=False,
                       output_dim=128):
    print('------------------------------------------------------------------')
    print('generator function defined')
    print('------------------------------------------------------------------')

    with tf.variable_scope('fully_connected_generator', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.1):
            with slim.arg_scope([slim.dropout], is_training=is_training):
                with slim.arg_scope([slim.batch_norm], decay=0.9, is_training=is_training, scale=True):
                    with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu,
                                        normalizer_fn=slim.batch_norm):

                        net = slim.fully_connected(inputs, 512, scope='ge_fc1')  # 512 in slim model # 1024 in fat model
                        # overwrite the training phase for the dropout and the noise layers to include the noise during the testing phase as well
                        net = slim.dropout(net, 0.5, is_training=True, scope='ge_dropout1')
                        net = noise(net, 1.0, phase=True, scope='ge_noise1')

                        net = slim.fully_connected(net, 512, scope='ge_fc2')
                        # overwrite the training phase for the dropout and the noise layers to include the noise during the testing phase as well
                        net = slim.dropout(net, 0.5, is_training=True, scope='ge_dropout2')
                        net = noise(net, 1.0, phase=True, scope='ge_noise2')

                        net = slim.fully_connected(net, 512, scope='ge_fc3')  # 512 in Slim model # 1024 in fat model
                        # overwrite the training phase for the dropout and the noise layers to include the noise during the testing phase as well
                        net = slim.dropout(net, 0.5, is_training=True, scope='ge_dropout3')
                        net = noise(net, 1.0, phase=True, scope='ge_noise3')

                        net = slim.fully_connected(net, output_dim, scope='ge_fc4')

                        return net

# ------------------------------------------------------------------------------
# Confusion function Ablation
# ------------------------------------------------------------------------------
@add_arg_scope
def residual_confusion1(inputs,
                        is_training=True,
                        reuse=False,
                        output_dim=512,
                        num_layers=3,
                        mid_activation='leaky_relu',
                        mid_layer_dim=512,
                        use_bnorm=False,
                        use_compact_feats=True):

    print('------------------------------------------------------------------')
    print('skip confusion function defined')
    print('------------------------------------------------------------------')

    assert inputs.shape[3] == output_dim, 'Residual confusin :the input and output dimensions of the confusion function should be same'

    # ------------------------------------------------------------------------------
    # Decide mid layer activation
    # ------------------------------------------------------------------------------
    if mid_activation == 'leaky_relu':
        mid_activation_fn = leaky_relu
    elif mid_activation == 'tanh':
        mid_activation_fn = tf.nn.tanh
    elif mid_activation == 'sigmoid':
        mid_activation_fn = tf.nn.sigmoid
    elif mid_activation == 'relu':
        mid_activation_fn = tf.nn.relu
    elif mid_activation == 'relu6':
        mid_activation_fn = tf.nn.relu6
    elif mid_activation == 'none':
        mid_activation_fn = None
    else:
        raise Exception, 'no such activation function defined'

    # ------------------------------------------------------------------------------
    # Decide to use batch normalization
    # ------------------------------------------------------------------------------
    if use_bnorm:
        mid_normalization_fn = slim.batch_norm
    else:
        mid_normalization_fn = None

    with tf.variable_scope('fully_connected_confusion_function', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.2):
            with slim.arg_scope([slim.dropout], is_training=is_training):
                with slim.arg_scope([slim.batch_norm], decay=0.9, is_training=is_training, scale=True):
                    with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=mid_activation_fn,
                                        normalizer_fn=mid_normalization_fn):

                        # If there are more than 1 layer
                        if num_layers > 1:
                            net = slim.fully_connected(inputs, mid_layer_dim, scope='cf_fc0')

                            # If there are more than 2 layers
                            if num_layers > 2:
                                for layer_index in range(num_layers-2):
                                    net = slim.fully_connected(net, mid_layer_dim, scope='cf_fc'+str(layer_index+1))

                            net = slim.fully_connected(net, output_dim, activation_fn=None, scope='cf_fc_out')
                            net = net + inputs

                        # If there is only one layer
                        else:
                            net = slim.fully_connected(inputs, output_dim, activation_fn=None, scope='cf_fc_out')
                            net = net + inputs

                        # ------------------------------------------------------------------------------
                        # Decide the activation of the final output
                        # ------------------------------------------------------------------------------
                        if use_compact_feats:
                            net = tf.nn.tanh(net)
                        else:
                            net = leaky_relu(net)

                        return net


@add_arg_scope
def plain_confusion(inputs,
                    is_training=True,
                    reuse=False,
                    output_dim=128,
                    num_layers=3,
                    mid_activation='leaky_relu',
                    mid_layer_dim=512,
                    use_bnorm=False,
                    use_compact_feats=True):

    print('------------------------------------------------------------------')
    print('plain confusion function defined')
    print('------------------------------------------------------------------')

    # ------------------------------------------------------------------------------------------------------------------
    # Decide mid layer activation
    # ------------------------------------------------------------------------------------------------------------------
    if mid_activation == 'leaky_relu':
        mid_activation_fn = leaky_relu
    elif mid_activation == 'tanh':
        mid_activation_fn = tf.nn.tanh
    elif mid_activation == 'sigmoid':
        mid_activation_fn = tf.nn.sigmoid
    elif mid_activation == 'relu':
        mid_activation_fn = tf.nn.relu
    elif mid_activation == 'relu6':
        mid_activation_fn = tf.nn.relu6
    elif mid_activation == 'none':
        mid_activation_fn = None
    else:
        raise Exception, 'no such activation function defined'

    # ------------------------------------------------------------------------------------------------------------------
    # Decide to use batch normalization
    # ------------------------------------------------------------------------------------------------------------------
    if use_bnorm:
        mid_normalization_fn = slim.batch_norm
    else:
        mid_normalization_fn = None

    with tf.variable_scope('fully_connected_confusion_function', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.2):
            with slim.arg_scope([slim.dropout], is_training=is_training):
                with slim.arg_scope([slim.batch_norm], decay=0.9, is_training=is_training, scale=True):
                    with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=mid_activation_fn,
                                        normalizer_fn=mid_normalization_fn):

                        # If there are more than 1 layer
                        if num_layers > 1:
                            net = slim.fully_connected(inputs, mid_layer_dim, scope='cf_fc0')

                            # If there are more than 2 layers
                            if num_layers > 2:
                                for layer_index in range(num_layers-2):
                                    net = slim.fully_connected(net, mid_layer_dim, scope='cf_fc'+str(layer_index+1))

                            # decide the last layer activation
                            if use_compact_feats:
                                net = slim.fully_connected(net, output_dim, activation_fn=tf.nn.tanh, scope='cf_fc_out')
                            else:
                                net = slim.fully_connected(net, output_dim, activation_fn=leaky_relu, scope='cf_fc_out')

                        # If there is only one layer
                        else:

                            # decide the last layer activation
                            if use_compact_feats:
                                net = slim.fully_connected(inputs, output_dim, activation_fn=tf.nn.tanh, scope='cf_fc_out')
                            else:
                                net = slim.fully_connected(inputs, output_dim, activation_fn=leaky_relu, scope='cf_fc_out')

                        return net

# =====================================================================================================================


def generator_function_clswgan(inputs,
                               is_training=True,
                               reuse=False,
                               output_dim=2048,
                               mid_dim=4096):
    print('------------------------------------------------------------------')
    print('generator function defined')
    print('------------------------------------------------------------------')

    with tf.variable_scope('fully_connected_generator', reuse=reuse):
        with slim.arg_scope([leaky_relu], a=0.1):
            with slim.arg_scope([slim.dropout], is_training=is_training):
                with slim.arg_scope([slim.batch_norm], decay=0.9, is_training=is_training, scale=True):
                    with slim.arg_scope([slim.conv2d, slim.fully_connected], activation_fn=leaky_relu,
                                        normalizer_fn=slim.batch_norm):

                        net = noise(inputs, 1.0, phase=True, scope='ge_noise1')
                        net = slim.fully_connected(net, mid_dim, scope='ge_fc1')
                        net = slim.fully_connected(net, output_dim, scope='ge_fc2')

                        return net











